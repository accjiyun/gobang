后续对AI进行了强化，并且更像一款专业的五子棋AI程序：http://git.oschina.net/accjiyun/GomokuAI

# GoBang
>一款包含人机对战（AI）、网络对战的五子棋小游戏。
>当时刚把Java的基础语法学完，因学长说的“项目驱动学习”的想法，于是决定做一个能实现人机对战的五子棋程序，并尽量让它的功能用起来更健全。


界面预览：

![](images/1.jpg)

![](images/2.jpg)

![](images/3.jpg)

![](images/4.jpg)

下面是对战截图：
号称“史上最聪明的五子棋”：

![](images/5.jpg)

清月连珠：
![](images/6.jpg)

Gomocup 5连冠军：弈心
![](images/7.jpg)