FROM java:7
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
CMD ["java", "-jar", "Server.jar"]