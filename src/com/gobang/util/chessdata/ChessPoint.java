﻿package com.gobang.util.chessdata;

import java.io.Serializable;

public class ChessPoint implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int x;
	int y;
	int nowPlay;//nowPlayer = 1为黑色,2为白色,-1为平局
	int sore;
	
	public int getSore() {
		return sore;
	}
	public void setSore(int sore) {
		this.sore = sore;
	}
	public ChessPoint(int x, int y, int nowPlay) {
		this.x = x;
		this.y = y;
		this.nowPlay = nowPlay;
	}
	public ChessPoint(int x, int y, int nowPlay, int sore) {
		this.x = x;
		this.y = y;
		this.nowPlay = nowPlay;
		this.sore = sore;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	public int getNowPlay() {
		return nowPlay;
	}
	public void setNowPlay(int nowPlay) {
		this.nowPlay = nowPlay;
	}
	public void  printChessPoint() {
		System.out.println("棋子坐标：" + this.getX() + "  "
				+ this.getY()+"  NowPlay:  "+this.nowPlay);
	}
	
}
