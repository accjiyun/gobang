﻿package com.gobang.util.uipattern;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Stack;
import java.util.Timer;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.gobang.gobangui.MainWindowUI;
import com.gobang.gobangui.PlayFrame;
import com.gobang.gobangui.ScreenModel;
import com.gobang.util.chessdata.ChessPoint;
import com.gobang.util.playeraction.CommonAction;

public class MyTableJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isShowOrder = true;

	final int screenHeight = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height);
	public ScreenModel screenModel = ScreenModel.MAX;// 屏幕的模式,分成大小屏幕
	int offsetImages = 23, mouseOffset = 34;;
	double gridWidth = 52.2857;
	int tableSize = 800, fontSize = 20;

	ImageIcon table = new ImageIcon(getClass()
			.getResource("/resources/background/table.jpg"));
	ImageIcon black = new ImageIcon(getClass()
			.getResource("/resources/background/black.png"));
	ImageIcon white = new ImageIcon(getClass()
			.getResource("/resources/background/white.png"));

	LinePoint linePoint = null;
	public boolean isWin = false;

	public int getMouseOffset() {
		return mouseOffset;
	}

	public ScreenModel getScreenModel() {
		return screenModel;
	}

	public void setShowOrder(boolean isShowOrder) {
		this.isShowOrder = isShowOrder;
	}

	public boolean isShowOrder() {
		return isShowOrder;
	}

	public boolean isWin() {
		return isWin;
	}

	public void setWin(boolean win) {
		this.isWin = win;
	}

	public MyTableJPanel() {
		if (MainWindowUI.getGameMode() == 2) {
			isShowOrder = false;
		}

		if (screenHeight < 800) {				//判断荧幕大小
			screenModel = ScreenModel.MIN;
			tableSize = 650;
			offsetImages = 19;
			mouseOffset = 27;
			gridWidth = 42.57;
			fontSize = 17;
			table = new ImageIcon(getClass().getResource("/resources/background/table_MIN.jpg"));
			black = new ImageIcon(getClass().getResource("/resources/background/black_MIN.png"));
			white = new ImageIcon(getClass().getResource("/resources/background/white_MIN.png"));
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		drawAllChess(g2d);
	}

	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		if(table != null) {
			g2d.drawImage(table.getImage(), 0, 0, tableSize, tableSize, null);			
		}

	}

	public void drawOneChess(ChessPoint chess){

		Graphics g= getGraphics();
		Graphics2D g2d = (Graphics2D) g;

		int showX = pointToMouse(chess.getX()) - offsetImages;
		int showY = pointToMouse(chess.getY()) - offsetImages;

		if (chess.getNowPlay() == 1) {
			g2d.drawImage(black.getImage(), showX, showY, null);
		} else if (chess.getNowPlay() == 2){
			g2d.drawImage(white.getImage(), showX, showY, null);
		}
	}

	public void drawAllChess(Graphics2D g2d) {

		setDoubleBuffered(true);
		g2d.setStroke(new BasicStroke(3));
		g2d.setFont(new Font("微软雅黑", Font.BOLD, fontSize));

		Stack <ChessPoint> chessStack = new Stack<ChessPoint>();
		int[][] Chessboard = new int[16][16];
		ChessPoint topChessPoint = null;
		int count = 0, numberOffset = 5;

		if (MainWindowUI.getGameMode() == 2) {
			chessStack = PlayFrame.getChessStack();
			Chessboard = PlayFrame.getChessboard();
		} else {
			chessStack = MainWindowUI.getLocalPlayFrame().getChessStack();
			Chessboard = MainWindowUI.getLocalPlayFrame().getChessboard();
		}

		if (!chessStack.empty()) {
			topChessPoint = chessStack.peek();
			linePoint = CommonAction.CheckWin(topChessPoint.getX(), 
					topChessPoint.getY(),topChessPoint.getNowPlay(),
					Chessboard);
			for (ChessPoint chessPoint : chessStack) {
				int showX = pointToMouse(chessPoint.getX()) - offsetImages;
				int showY = pointToMouse(chessPoint.getY()) - offsetImages;
				if (count >= 99) {
					numberOffset = 16;
				} else if (count >= 9) {
					numberOffset = 10;
				}
				if (chessPoint.getNowPlay() == 1) {
					g2d.drawImage(black.getImage(), showX, showY, null);
					if (isShowOrder || linePoint != null) {
						g2d.setColor(Color.WHITE);
						g2d.drawString(String.valueOf(++count), pointToMouse(chessPoint.getX()) - numberOffset, 
								pointToMouse(chessPoint.getY()) + 8);
					}
				} else if (chessPoint.getNowPlay() == 2){
					g2d.drawImage(white.getImage(), showX, showY, null);
					if (isShowOrder || linePoint != null) {
						g2d.setColor(Color.BLACK);
						g2d.drawString(String.valueOf(++count), pointToMouse(chessPoint.getX()) - numberOffset, 
								pointToMouse(chessPoint.getY()) + 8);
					}
				}
			}

			if (topChessPoint != null) {

				g2d.setColor(Color.RED);
				if (isShowOrder) {
					g2d.drawString(String.valueOf(count), pointToMouse(topChessPoint.getX()) - numberOffset, 
							pointToMouse(topChessPoint.getY()) + 8);
				} else {
					int showRoodcenterX = pointToMouse(topChessPoint.getX());
					int showRoodcenterY = pointToMouse(topChessPoint.getY());
					g2d.setColor(Color.red);
					g2d.setStroke(new BasicStroke(2));
					g2d.fillRect(showRoodcenterX - 3, showRoodcenterY - 2, 8, 8);
				}
			}
			
			if (isWin || linePoint != null) {// 有人赢棋了,只要判断一次，以后都不用了
				g2d.setColor(Color.RED);
				g2d.drawString(String.valueOf(count), pointToMouse(topChessPoint.getX()) - numberOffset, 
						pointToMouse(topChessPoint.getY()) + 8);
				drawLine(linePoint.getBeginX(), linePoint.getBeginY(),
						linePoint.getEndX(), linePoint.getEndY());
				
				if (!isWin) {
					isWin = true;
					CommonAction.WinInformation(topChessPoint.getNowPlay());
				}
			}
		}
	}

	public int pointToMouse(int point){
		return (int)( (point - 1) * gridWidth + mouseOffset );
	}

	public int coDispose(double number) {              //光标就近处理
		return (int)( (number/gridWidth) + 1.5 );
	}

	public void drawLine(int beginX, int beginY, int endX, int endY) {

		Graphics g= getGraphics();
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(new Color(0, 255, 0, 150));
		g2d.setStroke(new BasicStroke(7, BasicStroke.CAP_ROUND, BasicStroke.CAP_ROUND));
		g2d.drawLine(pointToMouse(beginX), pointToMouse(beginY), pointToMouse(endX), pointToMouse(endY));

	}

//	public void drawMouseFrame() {
//		Graphics g= getGraphics();
//		Graphics2D g2d = (Graphics2D) g;
//		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		g2d.setColor(Color.darkGray);
//		g2d.setStroke(new BasicStroke(5));
//
//		final int radius = 27;
//		int showX = pointToMouse(coDispose(getMousePosition().getX()) - 1) - radius;
//		int showY = pointToMouse(coDispose(getMousePosition().getY()) - 1) - radius;
//		g2d.drawRect(showX, showY, radius*2, radius*2);
//	}

	public void updateChessOnce(int gameMode){
		new Thread() {
			public void run() {
				try {
					sleep(40);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Graphics g= getGraphics();
				Graphics2D g2d = (Graphics2D) g;
				drawAllChess(g2d);
			}
		}.start();
	}

	public void updateChessLoop(int gameMode){
		Timer timer = new Timer();
		timer.schedule(new TimerTaskUpdateChess(), 0, 140);
	}

	class TimerTaskUpdateChess extends java.util.TimerTask{

		@Override
		public void run() {
			if (!isWin) {
				repaint();
			}
		}
	}

}
