﻿package com.gobang.util.uipattern;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.gobang.gobangui.PlayFrame;

public class FaceWindow extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;// 主容器
	private JScrollPane JSPanel;// 滚动面板

	private JLabel[] label = new JLabel[137];// 标签数组

	private PlayFrame playFrame;// 哪个窗体调用的

	public FaceWindow(PlayFrame playFrame) {
		this.playFrame = playFrame;
		initializeUI();
	}

	private void initializeUI() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 300);
		setUndecorated(true);
		setAlwaysOnTop(true);// 所有窗体的最前面

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(new GridLayout(12, 0));

		JSPanel = new JScrollPane(contentPane,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(JSPanel, BorderLayout.CENTER);

		String imagePath;
		for (int i = 0; i < label.length; i++) {
			imagePath = "/resources/expression/" + i + ".gif";
			label[i] = new JLabel(new FaceIcon(FaceWindow.class.getResource(imagePath), i), 
					SwingConstants.CENTER);
			label[i].setBorder(BorderFactory.createLineBorder(new Color(225, 225, 225), 1));
			label[i].setToolTipText(":)" + i);

			label[i].addMouseListener(new MouseAdapter() {
				@Override
				public void mouseEntered(MouseEvent e) {
					JLabel temp = (JLabel) e.getSource();
					temp.setBorder(BorderFactory.createLineBorder(Color.BLUE));
				}

				@Override
				public void mouseExited(MouseEvent e) {
					JLabel temp = (JLabel) e.getSource();
					temp.setBorder(BorderFactory.createLineBorder(new Color(225, 225, 225), 1));
				}

				@Override
				public void mouseClicked(MouseEvent e) {
					JLabel temp = (JLabel) e.getSource();
					FaceIcon expressionIco = (FaceIcon) temp.getIcon();
					playFrame.insertImageOnSendPanel(expressionIco);
					FaceWindow.this.setVisible(false);
					playFrame.setFaceWindowNull();
					FaceWindow.this.dispose();
				}
			});
			contentPane.add(label[i]);
		}
		this.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				FaceWindow.this.setVisible(false);
				playFrame.setFaceWindowNull();
				FaceWindow.this.dispose();
			}
		});
	}
}
