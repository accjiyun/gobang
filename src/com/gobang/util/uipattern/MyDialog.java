﻿package com.gobang.util.uipattern;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class MyDialog {

	static int value = 0;
	//type = 1 确认框 ， type = 2 选择框 
	public int showMessageDialog(String strTitle, final String strMessage, int type) {  
		UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("楷体", Font.BOLD, 25)));
		JLabel labelOfDialog = new JLabel(strMessage);
		labelOfDialog.setFont(new Font("楷体", Font.BOLD, 25));
		labelOfDialog.setLocation(new Point(500, 600));
		labelOfDialog.setPreferredSize(new Dimension(420, 100));
		labelOfDialog.setSize(new Dimension(600, 350));
		
		if (type == 2) {
			String pathImage = new String("/resources/hint/hint.jpg");
			if (strMessage.contains("打败") || strMessage.contains("能赢")) {
				pathImage = "/resources/hint/win.jpg";
			} else if (strMessage.contains("输")) {
				pathImage = "/resources/hint/fail.jpg";
			}
			value = JOptionPane.showConfirmDialog(null, labelOfDialog, strTitle, 
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, 
					new ImageIcon(getClass().getResource(pathImage)));
			if (value == JOptionPane.YES_OPTION) {
				return 1;
			} else if (value == JOptionPane.NO_OPTION){
				return 2;
			}
			else {
				return 0;
			}
		} else {
			new Thread() {
				public void run() {
					String pathImage = new String("/resources/hint/hint.jpg");
					if (strMessage.contains("打败") || strMessage.contains("能赢")) {
						pathImage = "/resources/hint/win.jpg";
					} else if (strMessage.contains("输")) {
						pathImage = "/resources/hint/fail.jpg";
					}
					value = JOptionPane.showConfirmDialog(null, labelOfDialog, strTitle, 
							JOptionPane.CLOSED_OPTION	, JOptionPane.OK_OPTION	, 
							new ImageIcon(getClass().getResource(pathImage)));
				}
			}.start();
			return 3;
		}
	}
}
