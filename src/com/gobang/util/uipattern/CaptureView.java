﻿package com.gobang.util.uipattern;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JWindow;

public class CaptureView extends JWindow implements MouseListener,KeyListener,MouseMotionListener{
	
	private static final long serialVersionUID = 1L;
	private BufferedImage desktopImg;
	private  boolean captured = false ,draging = false;
	private int x = 0 , y = 0 ,x1 = 0 , y1 = 0 , x2 = 1 , y2 = 1;	//坐标原点坐标,选区左上角和右下角坐标
	private int point_x , point_y;	//鼠标点坐标
	private DesktopCapture window;	//所属截图工具主窗体
	
	CaptureView(DesktopCapture window, BufferedImage img){
		
		this.window = window;
		this.desktopImg = img;
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
		init();
		setVisible(true);
		setAlwaysOnTop(true);
		this.requestFocus();
	}
	
	void init(){
		this.setContentPane(new BackGroundPanel(desktopImg));
		setLayout(null);
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	//桌面屏幕更新
	public void refreshBackGround(BufferedImage img){
		this.desktopImg = img;
		this.setContentPane(new BackGroundPanel(desktopImg));
		setVisible(true);
		setAlwaysOnTop(true);
		this.requestFocus();
	}
	
	public void paint(Graphics g){
		super.paint(g);
		g.setColor(Color.BLACK);
		if(captured == true){
			if(draging){
				//截图辅助十字线
				g.drawLine(point_x, 0, point_x, getHeight());
				g.drawLine(0, point_y, getWidth(), point_y);
			}
			confirmArea();	//确定截图选区的左上角坐标(x1,y1)和右下角坐标(x2,y2)
			if(x1<x2 && y1<y2)
			g.drawImage(desktopImg.getSubimage(x1, y1, Math.abs(x2-x1), Math.abs(y2-y1)), x1, y1, null);
			g.drawRect(x1,y1,Math.abs(x2-x1),Math.abs(y2-y1));
		}else{
			g.drawLine(point_x, 0, point_x, getHeight());
			g.drawLine(0, point_y, getWidth(), point_y);
		}
	}
	

	//确定区域的左上点和右下角坐标
	public void confirmArea(){
		int temp;
		//以x,y为截图选区左上角坐标初值,计算左上角x1,y1和右下角x2,y2的坐标
		x1 = x;
		y1 = y;
		if(x2 < x1){//2,3
			if(y2 < y1){ //2
				temp = x1;
				x1 = x2;
				x2 = temp;
				temp = y1;
				y1 = y2;
				y2 = temp;
			}else{	//4
				temp = x1;
				x1 = x2;
				x2 = temp;
			}
		}else{ //1,4
			if(y2 < y1){ //1
				temp = y1;
				y1 = y2;
				y2 = temp;
			}
		}
	}

	public void exit(){
		
		x = 0;
		y = 0;
		x1 = 0;
		y1 = 0;
		x2 = 1;
		y2 = 1;
		point_x = 0;
		point_y = 0;
		captured = false;
		draging = false;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		//双击左键,确定选择图像选区,保存截图
		if(e.getButton() == MouseEvent.BUTTON1){
			if(e.getClickCount() == 2){
				this.setVisible(false);
				window.saveCapture(x1, y1, x2, y2);
				exit();
				this.dispose();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		//左键在未选区完毕时单击,确定选区起点
		if(e.getButton() == MouseEvent.BUTTON1){
			if(captured == false){
				point_x = e.getX();
				point_y = e.getY();
				x = point_x;	//选区起点坐标
				y = point_y;
				draging = true;
				captured = true;
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		//左键释放,确定截图选区
		//右键释放,重新选区
		if(e.getButton() == MouseEvent.BUTTON1){
		if(draging == true){
			point_x = e.getX();
			point_y = e.getY();
			x2 = point_x;
			y2 = point_y;
			repaint();
			draging = false;
		}
		}else{
			draging = false;
			captured = false;
			point_x = e.getX();
			point_y = e.getY();
			repaint();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		//拖拉截图时画矩形
		if(draging == true){
			point_x = e.getX();
			point_y = e.getY();
			x2 = point_x;
			y2 = point_y;
			repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		//未决定截图时,画交叉线
		if(!captured){
			point_x = e.getX();
			point_y = e.getY();
			repaint();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getKeyCode() == KeyEvent.VK_CANCEL){
			this.setVisible(false);
			this.dispose();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		System.out.println("aaa");
		if(e.getKeyCode() == KeyEvent.VK_A){
			this.setVisible(false);
			this.dispose();
		}
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
			this.setVisible(false);
			this.dispose();
		}
	}

}
