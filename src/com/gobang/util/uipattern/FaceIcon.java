﻿package com.gobang.util.uipattern;

import java.net.URL;

import javax.swing.ImageIcon;

public class FaceIcon extends ImageIcon {
	private static final long serialVersionUID = 1L;
	/**
	 * 图片编号
	 */
	private int number = 0;
	private URL url;
	
	public FaceIcon(URL url, int number) {
		super(url);
		this.url = url;
		this.number = number;
	}
	
	public URL getUrl() {
		return url;
	}
	
	/**
	 * 获取图片编号
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * 重置图片编号
	 */
	public void setNumber(int number) {
		this.number = number;
	}
}