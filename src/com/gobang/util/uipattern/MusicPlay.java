﻿package com.gobang.util.uipattern;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Frame;
import java.io.File;
import java.net.URL;

public class MusicPlay extends Frame{

	private static final long serialVersionUID = 1L;
	URL uRLStartWAV = (URL)getClass().getResource("/resources/sounds/start.wav");
	URL uRLPlayChessWAV = (URL)getClass().getResource("/resources/sounds/playChess.wav");
	URL uRLWinWAV = (URL)getClass().getResource("/resources/sounds/win.wav");
	URL uRLFailWAV = (URL)getClass().getResource("/resources/sounds/fail.wav");
	URL uRLOpenWAV = (URL)getClass().getResource("/resources/sounds/open.wav");

	File fileStartWAV = new File("sounds/start.wav");
	File filePlayChessWAV = new File("sounds/playChess.wav");
	File fileWinWAV = new File("sounds/win.wav");
	File fileFailWAV = new File("sounds/fail.wav");
	File fileOpenWAV = new File("sounds/open.wav");

	AudioClip WAV;

	public MusicPlay(int type) {   //1为开始音乐，2是下棋声， 3是赢棋声，4是输棋声, 5是切换界面
		URL path = null;
		if (type == 1) {
			path = uRLStartWAV;
		} else if (type == 2) {
			path = uRLPlayChessWAV;
		} else if (type == 3) {
			path = uRLWinWAV;
		} else if (type == 4) {
			path = uRLFailWAV;
		} else if (type == 5) {
			path = uRLOpenWAV;
		}
		WAV = Applet.newAudioClip(path);
		WAV.play();
	}

	public void stopPlay() {
		WAV.stop();
	}

}
