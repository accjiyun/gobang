﻿package com.gobang.util.playeraction;

import com.gobang.netmode.clientdata.ChessClient;
import com.gobang.netmode.clientdata.ClientReceiveThread;
import com.gobang.util.uipattern.MyDialog;



public class CompetitorAction {
	

	public static void askedForBeginDialog(){
		/*
		 * 用户被询问是否要同意开始，根据用户的选择，根据用户的选择调用函数
		 * begin(boolean b);
		 */
		MyDialog myDialog = new MyDialog();
		int value = myDialog.showMessageDialog("提示", "房主请求开始下棋，是否同意？", 2);
		if (value == 1) {
			ChessClient.sendBegin(true);
			ClientReceiveThread.getPlayFrame().rePlay();
		} else if (value == 2) {
			ChessClient.sendBegin(false);
		} else {
			askedForBeginDialog();
		}
	}

}
