﻿package com.gobang.util.playeraction;

import com.gobang.gobangui.PlayFrame;
import com.gobang.netmode.clientdata.ClientReceiveThread;
import com.gobang.util.uipattern.MyDialog;



public class RoomOwnerAction {

	public static void  answerForBegin(boolean b) {
		/*
		 * 得到对方的回应
		 */
		MyDialog myDialog = new MyDialog();
		if (b) {
			myDialog.showMessageDialog("提示", "对方同意开始下棋。", 1);
			ClientReceiveThread.getPlayFrame().rePlay();
			PlayFrame.setCanPlay(true);
		} else {
			myDialog.showMessageDialog("提示", "对方不同意开始下棋。", 1);
		}
	}
}
