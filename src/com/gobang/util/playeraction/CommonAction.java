﻿package com.gobang.util.playeraction;

import com.gobang.gobangui.PlayFrame;
import com.gobang.gobangui.MainWindowUI;
import com.gobang.netmode.clientdata.ChessClient;
import com.gobang.netmode.clientdata.ClientReceiveThread;
import com.gobang.util.uipattern.LinePoint;
import com.gobang.util.uipattern.MusicPlay;
import com.gobang.util.uipattern.MyDialog;

public class CommonAction {

	/*********************************************************************************************************/
	/**
	 * 收到请求信息
	 */

	public static void  askedForBackDialog() {
		/*
		 * 用户被询问是否要同意悔棋，根据用户的选择，根据用户的选择调用函数
		 * back(boolean b);
		 */
		MyDialog myDialog = new MyDialog();
		int value = myDialog.showMessageDialog("提示", "对方请求悔棋，是否同意？", 2);
		if (value == 1) {
			ClientReceiveThread.getPlayFrame().youselfAgreeBack();
			PlayFrame.setCanPlay(false);
			ChessClient.sendBack(true);
		} else if (value == 2) {
			ChessClient.sendBack(false);
		} else {
			askedForBackDialog();
		}

	}

	public static void askedForPeaceDialog(){
		/*
		 * 用户被询问是否要同意和棋根据用户的选择，在根据用户的选择调用函数
		 * peace(boolean b);
		 */
		MyDialog myDialog = new MyDialog();
		int value = myDialog.showMessageDialog("提示", "对方请求和棋，是否同意？", 2);
		if (value == 1) {
			PlayFrame.setCanPlay(false);
			ChessClient.setBegin(false);
			ChessClient.sendPeace(true);
		} else if (value == 2) {
			ChessClient.sendPeace(false);
		} else {
			askedForPeaceDialog();
		}
	}

	/**************************************************************************************************************/
	/**
	 *                   发出的请求收到回复
	 * @param b
	 */
	
	public static void  otherGiveUp() {
		MyDialog myDialog = new MyDialog();
		myDialog.showMessageDialog("喜讯", "你太厉害了，对方无奈的认输了！", 1);
	}

	public static void  answerForgiveUp() {
		PlayFrame.setCanPlay(false);
		MyDialog myDialog = new MyDialog();
		myDialog.showMessageDialog("喜讯", "你太厉害了，对方无奈的认输了！", 1);
	}

	public static void  answerForBack(boolean b) {
		/*
		 * 得到对方的回应
		 */
		MyDialog myDialog = new MyDialog();
		if (b) {
			myDialog.showMessageDialog("提示", "对方同意悔棋。", 1);
		} else {
			myDialog.showMessageDialog("提示", "对方不同意悔棋。", 1);
		}
	}

	public static void  answerForPeace(boolean b) {
		/*
		 * 得到对方的回应
		 */
		MyDialog myDialog = new MyDialog();
		if (b) {
			PlayFrame.setCanPlay(false);
			myDialog.showMessageDialog("提示", "对方同意和棋。", 1);
		} else {
			myDialog.showMessageDialog("提示", "对方不同意和棋。", 1);
		}

	}

	public static void rePlayInformation(boolean b){
		MyDialog myDialog = new MyDialog();
		if(b){
			myDialog.showMessageDialog("提示", "对方同意重新开局", 1);
			ClientReceiveThread.getPlayFrame().rePlay();
		}
		else{
			myDialog.showMessageDialog("提示", "对方不同意重新开局", 1);
		}

	}

	/********************************************************************************************/
	/**
	 *                              
	 * @param b
	 */


	/***********************************************************************************************/


	/**
	 *                      判断赢棋函数
	 * @param pointX
	 * @param pointY
	 * @param nowPlayer
	 * @param Chessboard
	 */
	public static LinePoint CheckWin(int pointX, int pointY, int nowPlayer, int[][] Chessboard) {  //计算四个方向的连续相同棋子数目
		final int N = 15;
		int i, upX = pointX, upY = pointY, downX = pointX, downY = pointY;
		/**
		 * 横向检测
		 */
		for(i = 1;pointX + i <= N &&  Chessboard[pointX + i][pointY] == nowPlayer; i++);  //向右检查
		upX = pointX + i - 1;
		for(i = 1; pointX - i > 0 && Chessboard[pointX - i][pointY] == nowPlayer; i++);   //向左检查
		downX = pointX - i + 1;		
		if (upX - downX >= 4 ){ 
			//			setWinFlag(nowPlayer, upX, pointY, downX, pointY);
			//			return true;
			return (new LinePoint(upX, pointY, downX, pointY));
		}
		/**
		 * 纵向检测
		 */
		for(i = 1; pointY + i <= N && Chessboard[pointX][pointY + i] == nowPlayer; i++); //向下检查
		downY = pointY + i - 1;
		for(i = 1; pointY - i> 0 && Chessboard[pointX][pointY - i ] == nowPlayer; i++); //向上检查
		upY = pointY - i + 1;
		if(downY - upY >= 4) {
			//			setWinFlag(nowPlayer, pointX, upY, pointX, downY);
			//			return true;
			return (new LinePoint(pointX, upY, pointX, downY));
		}
		/**
		 *   "\"斜检测
		 */
		for(i = 1; pointX + i<= N && pointY + i <= N && Chessboard[pointX+i][pointY+i] == nowPlayer; i++); //向右下检查
		downX = pointX + i - 1;
		downY = pointY + i - 1;
		for(i = 1; pointX - i > 0 && pointY - i > 0 && Chessboard[pointX-i][pointY-i] == nowPlayer; i++); //向左上检查
		upX = pointX - i + 1;
		upY = pointY - i + 1;
		if(downX - upX >= 4  || downY -upY >= 4) {
			//			setWinFlag(nowPlayer, upX, upY, downX, downY);
			//			return true;
			return (new LinePoint(upX, upY, downX, downY));
		}
		/**
		 *   "/"斜检测
		 */
		for(i = 1; pointX + i <= N && pointY - i > 0 && Chessboard[pointX+i][pointY-i] == nowPlayer; i++); //向右上检查
		upX = pointX + i - 1;
		upY = pointY - i + 1;
		for(i = 1; pointX - i > 0 && pointY + i <= N && Chessboard[pointX-i][pointY+i] == nowPlayer; i++); //向左下检查
		downX = pointX - i + 1;
		downY = pointY + i - 1;
		if(upX - downX >= 4  || downY - upY >= 4) {
			//			setWinFlag(nowPlayer, upX, upY, downX, downY);
			//			return true;
			return (new LinePoint(upX, upY, downX, downY));
		}
		//		return false;
		return null;
	}

	//	public static void setWinFlag(int nowPlayer, int beginX, int beginY, int endX, int endY) {
	//		if (mainWindowUI.getGameMode() == 2) {
	//			PlayFrame.setWinner(nowPlayer);
	//			PlayFrame.getPanelTable().drawLine(beginX, beginY, endX, endY);
	//		} else {
	//			mainWindowUI.getLocalPlayFrame().setWinner(nowPlayer);
	//			mainWindowUI.getLocalPlayFrame().getPanelTable().drawLine(beginX, beginY, endX, endY);
	//		}
	//	}

	public static void WinInformation(int nowPlayer) {

//		int[][] chessBoard = MainWindowUI.getLocalPlayFrame().getChessboard();
//		for (int j = 1; j <= 15; j++) {
//			for (int i = 1; i <= 15; i++) {
//				switch (chessBoard[i][j]) {
//				case 1 :System.err.print(" ● ");break;
//				case 2: System.err.print(" ○ ");break;
//				default : System.err.print("【】");break;
//				}
//			}
//			System.err.println("");
//		}
//		System.err.println("done");
		int gameMode = MainWindowUI.getGameMode();
		int value;
		MyDialog myDialog = new MyDialog();
		String[] massages = {"提示", "<html>恭喜你打败了Ta！<br>是否再来一局</html>",
				"<html>你输了~~<br>是否再杀一局?</html>","黑棋赢了！",
				"白棋赢了！", "这场比赛，无人能赢！"};
		if (gameMode == 1) {
			if (nowPlayer == MainWindowUI.getLocalPlayFrame().getComputerRole()) {
				new MusicPlay(4);
				value = myDialog.showMessageDialog(massages[0], "你输了，下次加油！是否再杀一局", 2);
			} else {
				new MusicPlay(3);
				value = myDialog.showMessageDialog(massages[0], 
						"<html>你实在太厉害了，这都能赢！<br>是否再杀一局</html>", 2);
			}
			if (value == 1) {
				MainWindowUI.getLocalPlayFrame().rePlay();
			} else {
				MainWindowUI.getLocalPlayFrame().setCanPlay(false);
			}
		} else if (gameMode == 2 && ChessClient.getIsBegin()) {//网络对战
//			ChessClient.isBegin = false;
			PlayFrame.setCanPlay(false);//锁定下棋状态
			int attribute = ChessClient.getAttribute();
			if (( attribute == 1 || attribute == 2 ) && (attribute == nowPlayer) ) {
				ChessClient.win();//本人赢棋了
				new MusicPlay(3);
				value = myDialog.showMessageDialog(massages[0], massages[1], 2);
				if (value == 1) {
					ClientReceiveThread.getPlayFrame().showNoticeIntable("等待对方的回应……", "通知");
					ChessClient.rePlay(true);//***********************************************	要调整		
				} else {
					ChessClient.rePlay(false);
				}

			} else if (( attribute == 1 || attribute == 2 ) && (attribute != nowPlayer) ) {
				new MusicPlay(4);
				value = myDialog.showMessageDialog(massages[0], massages[2], 2);
				if (value == 1) {
					ClientReceiveThread.getPlayFrame().showNoticeIntable("等待对方的回应……", "通知");
					ChessClient.rePlay(true);
				} else {
					ChessClient.rePlay(false);
				}
			} else if (attribute == 3){
				new MusicPlay(3);
				if (nowPlayer == 1){
					myDialog.showMessageDialog(massages[0], massages[3], 1);
				} else if (nowPlayer == 2) {
					myDialog.showMessageDialog(massages[0], massages[4], 1);
				} else if (nowPlayer == 3){
					myDialog.showMessageDialog(massages[0], massages[5], 1);
				}
			}

		} else if (gameMode == 3){
			new MusicPlay(3);
			if (nowPlayer == 1){
				myDialog.showMessageDialog(massages[0], massages[3], 1);
			} else if (nowPlayer == 2) {
				myDialog.showMessageDialog(massages[0], massages[4], 1);
			} else if (nowPlayer == 3){
				myDialog.showMessageDialog(massages[0], massages[5], 1);
			}
			MainWindowUI.getLocalPlayFrame().setCanPlay(false);
		}		
	}
}


