﻿package com.gobang.gobangui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import javax.swing.JButton;

import com.gobang.netmode.search.SearchJFrameOfLAN;
import com.gobang.netmode.search.SearchJFrameOfWAN;
import com.gobang.netmode.search.getLocalIP;
import com.gobang.netmode.util.Netmode;
import com.gobang.util.uipattern.MyDialog;
import com.gobang.util.uipattern.MyJButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ServerSelect extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	
	int windowStartX = (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width)/2 - 550;
	int windowStartY = (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().height)/2 - 450;
	private static int mouseOnSceenX, mouseOnSceenY;
	private static int jframeX, jframeY;
	
	

	/**
	 * Create the frame.
	 */
	public ServerSelect() {
		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		setBounds(windowStartX, windowStartY, 1024, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel(){
			private static final long serialVersionUID = 1L;
			@Override
			protected void paintComponent(Graphics g) {
				// TODO Auto-generated method stub
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				
				ImageIcon backGround = new ImageIcon(getClass().getResource("/resources/background/background5.png"));
				g2d.drawImage(backGround.getImage(), 0, 0, contentPane.getWidth(), contentPane.getHeight(), contentPane);

//				g2d.setClip(0, 0, getWidth(), 45);
//				g2d.setColor(Color.white);
//				g2d.fillRoundRect(0, 3, getWidth() - 1, getHeight() - 1, 20, 20);
//				g2d.setClip(null);
				
				g2d.setColor(Color.darkGray);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRoundRect(320, 200, 380, 400, 20, 20);
				
				g2d.setFont(new Font("楷体", Font.BOLD, 27));
				g2d.drawString("服务器选择", (getWidth()/2)-50, 35);
				
				g2d.drawRoundRect(1, 1, getWidth() - 3, getHeight() - 3, 20, 20);
			}
		};
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				if (getWidth() != (int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()) {
					setLocation(jframeX + (arg0.getXOnScreen() - mouseOnSceenX), jframeY + (arg0.getYOnScreen() - mouseOnSceenY));
				}
			}
		});
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				mouseOnSceenX = arg0.getXOnScreen();
				mouseOnSceenY = arg0.getYOnScreen();
				jframeX = getX();
				jframeY = getY();
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnInternet = new MyJButton();
		btnInternet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							     SearchJFrameOfWAN searchframe = new SearchJFrameOfWAN();
								 MainWindowUI.setSearchframe(searchframe);
								 MainWindowUI.setNetmode(Netmode.WAN);
								 setVisible(false);
								 searchframe.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnInternet.setFont(new Font("楷体", Font.PLAIN, 30));
		btnInternet.setText("\u4E92\u8054\u7F51\u6A21\u5F0F");
		JButton btnRegionNetwork = new MyJButton();
		btnRegionNetwork.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if(getLocalIP.filterIP(getLocalIP.getLocalIPListInteAddresses()).size() > 0){
								SearchJFrameOfLAN searchframe = new SearchJFrameOfLAN();
								 MainWindowUI.setSearchframe(searchframe);
								 MainWindowUI.setNetmode(Netmode.LAN);
								 setVisible(false);
								 searchframe.setVisible(true);
							} else{
								MyDialog myDialog = new MyDialog();
								myDialog.showMessageDialog("警告", "<html>无法获取局域网网关,<br>请检查网络后重试！</html>", 1);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnRegionNetwork.setFont(new Font("楷体", Font.PLAIN, 30));
		btnRegionNetwork.setText("\u5C40\u57DF\u7F51\u6A21\u5F0F");
		JButton btnBackToMain = new MyJButton();
		btnBackToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				MainWindowUI.getMainFrame().setVisible(true);
			}
		});
		btnBackToMain.setFont(new Font("楷体", Font.PLAIN, 30));
		btnBackToMain.setText("\u56DE\u5230\u4E3B\u754C\u9762");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(385)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnBackToMain, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
						.addComponent(btnInternet, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
						.addComponent(btnRegionNetwork, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE))
					.addGap(375))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(233)
					.addComponent(btnInternet, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addGap(44)
					.addComponent(btnRegionNetwork, GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
					.addGap(53)
					.addComponent(btnBackToMain, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addGap(183))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
