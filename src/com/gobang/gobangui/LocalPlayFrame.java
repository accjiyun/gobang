﻿package com.gobang.gobangui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Stack;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.gobang.aimode.aiengine.GoBangAI;
import com.gobang.util.chessdata.ChessPoint;
import com.gobang.util.uipattern.MusicPlay;
import com.gobang.util.uipattern.MyDialog;
import com.gobang.util.uipattern.MyJButton;
import com.gobang.util.uipattern.MyTableJPanel;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class LocalPlayFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private int maxDepth;
	private int maxWight;
	private int computerRole = 1;
	private int[][] chessboard  = new int[16][16];
	private int nowPlayer = 1; //1为黑色,2为白色,-1为平局
	private double mouseOnTableX, mouseOnTableY;
	private int pointOntableX, pointOnTableY;
	private Stack<ChessPoint> chessStack = new Stack<ChessPoint>(); //棋子堆栈
	private int count;  //回合数
	private int regretOpportunity;
	private boolean isCanPlay = true;
	private boolean isPlaying = false;

	private int mouseOnSceenX, mouseOnSceenY;
	private int jframeX, jframeY;
	final int screenWidth = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().width);
	final int screenHeight = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height);
	private int windowStartX = screenWidth/2 - 750;
	private int windowStartY = screenHeight/2 - 520;
	private int windowWidth = 1500;
	private int windowHeight = 1000;
	private int chessTableSize = 800;
	
	private JPanel contentPane;
	private JPanel panelPlayChessOperate;
	private MyTableJPanel panelTable;
	private JPanel panelWindowOperate;
	private JPanel panelMain;
	private JPanel panelGameMode;
	private JRadioButton radioButtonShowOrder;
	private MyJButton buttonMax;
	private MyJButton buttonExit;
	private MyJButton buttonMin;
	private MyJButton buttonBack;
	private MyJButton buttonStart;
	private JRadioButton radioButtonFool;
	private JRadioButton radioButtonEasy;
	private JRadioButton radioButtonNormal;
	private JRadioButton radioButtonDifficult;
	private JRadioButton radioButtonComputer;
	private JRadioButton radioButtonPlayer;
	private JPanel panelFirstMove;
	private MyJButton buttonPlayAgain;
	private MyJButton buttonRetract;
	private ButtonGroup gameLevel;
	private ButtonGroup firstMove;

	public int getCount() {
		return count;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public int getMaxWight() {
		return maxWight;
	}
	public int getComputerRole() {
		return computerRole;
	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setChessboard(int[][] chessboard) {
		this.chessboard = chessboard;
	}

	public int[][] getChessboard() {
		return chessboard;
	}

	public int getNowPlayer() {
		return nowPlayer;
	}

	public void setNowPlayer(int nowPlayer) {
		this.nowPlayer = nowPlayer;
	}

	public MyTableJPanel getPanelTable() {
		return panelTable;
	}

	public Stack<ChessPoint> getChessStack() {
		return chessStack;
	}

	public void setCanPlay(boolean isCanPlay) {
		this.isCanPlay = isCanPlay;
	}

	/**
	 * Create the frame.
	 */
	public LocalPlayFrame() {

		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(windowStartX, windowStartY, windowWidth, windowHeight);
		contentPane = new JPanel(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				ImageIcon backGround = new ImageIcon(
						getClass().getResource("/resources/background/background6.png"));
				g2d.drawImage(backGround.getImage(), 0, 0, 
						contentPane.getWidth(), contentPane.getHeight(), contentPane);

				g2d.setColor(new Color(255, 255, 255, 120));
				g2d.fillRoundRect(3, 3, getWidth() - 6, getHeight() - 6, 20, 20);

				g2d.setClip(0, 0, getWidth(), 45);
				g2d.setColor(Color.white);
				g2d.fillRoundRect(0, 3, getWidth() - 1, getHeight() - 1, 20, 20);
				g2d.setClip(null);

				g2d.setColor(Color.darkGray);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRoundRect(1, 1, getWidth() - 3, getHeight() - 3, 20, 20);


				g2d.setFont(new Font("楷体", Font.BOLD, 27));
				if (MainWindowUI.getGameMode() == 1) {
					g2d.drawString("五子棋人机对战", (getWidth()/2)-80, 35);
				} else if (MainWindowUI.getGameMode() == 3){
					g2d.drawString("五子棋人人对战", (getWidth()/2)-80, 35);
				}
			}

		};
		contentPane.setOpaque(false);
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (getWidth() != screenWidth
						|| getHeight() != screenHeight) {
					setLocation(jframeX + (e.getXOnScreen() - mouseOnSceenX), 
							jframeY + (e.getYOnScreen() - mouseOnSceenY));
				}
			}
		});
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mouseOnSceenX = e.getXOnScreen();
				mouseOnSceenY = e.getYOnScreen();
				jframeX = getX();
				jframeY = getY();
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		panelMain = new JPanel();
		panelMain.setOpaque(false);
		panelMain.setBounds(0, 58, getWidth(), getHeight() - 50);

		panelPlayChessOperate = new JPanel();
		panelPlayChessOperate.setOpaque(false);

		buttonRetract = new MyJButton();
		buttonRetract.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonRetract.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MyDialog myDialog = new MyDialog();
				if (isPlaying == false) {
					return;
				}
				if (regretOpportunity > 10) {
					myDialog.showMessageDialog("提示", "<html>难得10颗后悔药！<br>你也能用完啊？？</html>", 1);
					return;
				}
				if (chessStack.isEmpty() || count < 2) {
					myDialog.showMessageDialog("提示", "才几步呢，你就悔棋？要不得!", 1);
					return;
				}
				if (count >= 255) {
					myDialog.showMessageDialog("提示", "和棋！", 1);
					return;
				}
				panelTable.setWin(false);
				regretOpportunity++;
				count = count - 2;
				int i = 2;
				while (i-- > 0) {
					ChessPoint chessPoint = (ChessPoint) chessStack.peek();
					chessboard[chessPoint.getX()][chessPoint.getY()] = 0;
					chessStack.pop();
				}
				panelTable.updateUI();
				isCanPlay = true;
			}
		});
		buttonRetract.setFont(new Font("楷体", Font.PLAIN, 33));
		buttonRetract.setText("悔棋");

		buttonPlayAgain = new MyJButton();
		buttonPlayAgain.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPlayAgain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (isPlaying == true) {
					rePlay();
				}
			}
		});
		buttonPlayAgain.setFont(new Font("楷体", Font.PLAIN, 33));
		buttonPlayAgain.setText("重新开局");

		panelTable = new MyTableJPanel();
		panelTable.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				//				panelTable.drawMouseFrame();
			}
		});
		panelTable.setOpaque(false);
		panelTable.setBounds(getWidth()/2 - 400, getHeight()/2 - (int)(getHeight()*0.474), chessTableSize, chessTableSize);
		panelTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (isPlaying == false || isCanPlay == false) {
					return;
				}

				mouseOnTableX = panelTable.getMousePosition().getX();
				mouseOnTableY = panelTable.getMousePosition().getY();
				//						System.out.println("绝对" + mouseOnTableX + ":" + mouseOnTableY);

				pointOntableX = panelTable.coDispose(mouseOnTableX - 34);
				pointOnTableY = panelTable.coDispose(mouseOnTableY - 34);
				//						System.out.println("相对：" + pointOntableX+ ":" + pointOnTableY);

				if (pointOntableX <= 15 && pointOntableX > 0
						&& pointOnTableY <= 15 && pointOnTableY > 0
						&& chessboard[pointOntableX][pointOnTableY] == 0
						&& isCanPlay && isPlaying) {
					ChessPoint chessPoint = new ChessPoint(pointOntableX, pointOnTableY, nowPlayer);
					chessStack.push(chessPoint);
					chessboard[pointOntableX][pointOnTableY] = nowPlayer;
					count++;
					nowPlayer = 3 - nowPlayer;  //转化角色
					new MusicPlay(2);
					panelTable.updateChessOnce(MainWindowUI.getGameMode());
				}
				if (MainWindowUI.getGameMode() == 1 && nowPlayer == computerRole && isCanPlay) {            //电脑下棋
					computerPlay();
				}
			}
		});

		panelWindowOperate = new JPanel();
		panelWindowOperate.setOpaque(false);
		panelWindowOperate.setBounds(getWidth() - 376, 4, 373, 46);
		panelPlayChessOperate.setBounds((getWidth()/2) - 315, (int)(getHeight() * 0.85), 629, 68);
		buttonMax = new MyJButton(3);
		buttonMax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (getWidth() == screenWidth) {
					setBounds(windowStartX, windowStartY, windowWidth, windowHeight);
					windowControl(1);
				} else {
					setBounds(0, 0, screenWidth, screenHeight);
					windowControl(2);
				}
			}
		});


		buttonMax.setFont(new Font("Yu Gothic UI", Font.PLAIN, 19));
		buttonMax.setText("MAX");

		buttonExit = new MyJButton(3);
		buttonExit.setText("Exit");
		buttonExit.setFont(new Font("Yu Gothic UI", Font.PLAIN, 19));
		buttonExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		buttonMin = new MyJButton(3);
		buttonMin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setExtendedState(JFrame.ICONIFIED);
			}
		});
		buttonMin.setText("MIN");
		buttonMin.setFont(new Font("Yu Gothic UI", Font.PLAIN, 19));

		JLabel labelIcon = new JLabel(" ");
		labelIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				boolean isNowShowOrder = radioButtonShowOrder.isSelected();
				radioButtonShowOrder.setSelected(!isNowShowOrder);
				panelTable.setShowOrder(!isNowShowOrder);
			}
		});
		labelIcon.setBounds(5, 5, 38, 37);
		labelIcon.setFont(new Font("宋体", Font.PLAIN, 36));
		labelIcon.setIcon(new ImageIcon(getClass().getResource("/resources/ico/GoBangIco.png")));
		contentPane.setLayout(null);
		contentPane.add(panelMain);
		panelMain.setLayout(null);
		panelMain.add(panelTable);
		panelMain.add(panelPlayChessOperate);
		GroupLayout gl_panelPlayChessOperate = new GroupLayout(panelPlayChessOperate);
		gl_panelPlayChessOperate.setHorizontalGroup(
				gl_panelPlayChessOperate.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelPlayChessOperate.createSequentialGroup()
						.addComponent(buttonRetract, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
						.addGap(276)
						.addComponent(buttonPlayAgain, GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE))
				);
		gl_panelPlayChessOperate.setVerticalGroup(
				gl_panelPlayChessOperate.createParallelGroup(Alignment.LEADING)
				.addComponent(buttonRetract, GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
				.addComponent(buttonPlayAgain, GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
				);
		panelPlayChessOperate.setLayout(gl_panelPlayChessOperate);

		firstMove = new ButtonGroup();

		gameLevel = new ButtonGroup();

		panelGameMode = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				g2d.setColor(Color.darkGray);
				g2d.setStroke(new BasicStroke(2));
				g2d.setFont(new Font("楷体", Font.BOLD, 40));
				g2d.drawString("先手", (getWidth()/2)-50, 40);
				g2d.drawString("难度", (getWidth()/2)-50, 280);
			}
		};
		panelGameMode.setOpaque(false);
		panelGameMode.setBounds(1183, 83, 267, 800);
		panelMain.add(panelGameMode);

		panelFirstMove = new JPanel(){
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				g2d.setColor(Color.darkGray);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRoundRect(1, 1, getWidth() - 3, getHeight() - 3, 20, 20);
			}
		};
		panelFirstMove.setOpaque(false);
		
		radioButtonComputer = new JRadioButton("\u7535\u8111");
		radioButtonComputer.setSelected(true);
		radioButtonComputer.setOpaque(false);
		radioButtonComputer.setFont(new Font("微软雅黑", Font.PLAIN, 25));
		
		radioButtonPlayer = new JRadioButton("\u73A9\u5BB6");
		radioButtonPlayer.setOpaque(false);
		radioButtonPlayer.setFont(new Font("微软雅黑", Font.PLAIN, 25));
		firstMove.add(radioButtonComputer);
		firstMove.add(radioButtonPlayer);
		
		GroupLayout gl_panelFirstMove = new GroupLayout(panelFirstMove);
		gl_panelFirstMove.setHorizontalGroup(
				gl_panelFirstMove.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelFirstMove.createSequentialGroup()
						.addGap(68)
						.addGroup(gl_panelFirstMove.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelFirstMove.createSequentialGroup()
										.addComponent(radioButtonPlayer, GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
										.addContainerGap())
										.addGroup(gl_panelFirstMove.createSequentialGroup()
												.addComponent(radioButtonComputer, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
												.addGap(93))))
				);
		gl_panelFirstMove.setVerticalGroup(
				gl_panelFirstMove.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelFirstMove.createSequentialGroup()
						.addContainerGap(23, Short.MAX_VALUE)
						.addComponent(radioButtonComputer, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(radioButtonPlayer)
						.addGap(22))
				);
		panelFirstMove.setLayout(gl_panelFirstMove);

		JPanel panelLevel = new JPanel(){
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				g2d.setColor(Color.darkGray);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRoundRect(1, 1, getWidth() - 3, getHeight() - 3, 20, 20);
			}
		};
		panelLevel.setOpaque(false);

		radioButtonEasy = new JRadioButton("\u7B80\u5355");
		radioButtonEasy.setSelected(true);
		radioButtonEasy.setOpaque(false);
		radioButtonEasy.setFont(new Font("微软雅黑", Font.PLAIN, 25));

		radioButtonNormal = new JRadioButton("\u4E2D\u7B49");
		radioButtonNormal.setOpaque(false);
		radioButtonNormal.setFont(new Font("微软雅黑", Font.PLAIN, 25));

		radioButtonDifficult = new JRadioButton("\u56F0\u96BE");
		radioButtonDifficult.setOpaque(false);
		radioButtonDifficult.setFont(new Font("微软雅黑", Font.PLAIN, 25));
		
		radioButtonFool = new JRadioButton("\u50BB\u74DC");
		radioButtonFool.setOpaque(false);
		radioButtonFool.setFont(new Font("微软雅黑", Font.PLAIN, 25));

		gameLevel.add(radioButtonFool);
		gameLevel.add(radioButtonEasy);
		gameLevel.add(radioButtonNormal);
		gameLevel.add(radioButtonDifficult);

		GroupLayout gl_panelLevel = new GroupLayout(panelLevel);
		gl_panelLevel.setHorizontalGroup(
				gl_panelLevel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelLevel.createSequentialGroup()
						.addGap(72)
						.addGroup(gl_panelLevel.createParallelGroup(Alignment.LEADING)
								.addComponent(radioButtonFool)
								.addGroup(gl_panelLevel.createParallelGroup(Alignment.TRAILING)
										.addComponent(radioButtonEasy)
										.addComponent(radioButtonNormal)
										.addComponent(radioButtonDifficult)))
										.addContainerGap(86, Short.MAX_VALUE))
				);
		gl_panelLevel.setVerticalGroup(
				gl_panelLevel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panelLevel.createSequentialGroup()
						.addGap(31)
						.addComponent(radioButtonFool)
						.addPreferredGap(ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
						.addComponent(radioButtonEasy)
						.addGap(30)
						.addComponent(radioButtonNormal)
						.addGap(36)
						.addComponent(radioButtonDifficult)
						.addGap(31))
				);
		panelLevel.setLayout(gl_panelLevel);

		buttonStart = new MyJButton();
		buttonStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initializeChessData();
				if (radioButtonFool.isSelected()) {
					maxDepth = 2;
					maxWight = 4;
				} else if (radioButtonEasy.isSelected()) {
					maxDepth = 4;
					maxWight = 16;
				} else if (radioButtonNormal.isSelected()) {
					maxDepth = 6;
					maxWight = 8;
				} else if (radioButtonDifficult.isSelected()) {
					maxDepth = 7;
					maxWight = 10;
				}
				isPlaying = true;
				isCanPlay = true;
				if (radioButtonComputer.isSelected()) {
					computerRole = 1;
					computerPlay();
				} else if (radioButtonPlayer.isSelected()) {
					computerRole = 2;
				}
			}
		});
		buttonStart.setText("\u5F00\u59CB\u6E38\u620F");
		buttonStart.setFont(new Font("楷体", Font.PLAIN, 27));

		radioButtonShowOrder = new JRadioButton("\u663E\u793A\u843D\u5B50\u6B21\u5E8F");
		radioButtonShowOrder.setSelected(true);
		radioButtonShowOrder.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				panelTable.setShowOrder(radioButtonShowOrder.isSelected());
			}
		});
		radioButtonShowOrder.setOpaque(false);
		radioButtonShowOrder.setFont(new Font("微软雅黑", Font.BOLD, 22));
		GroupLayout gl_panelGameMode = new GroupLayout(panelGameMode);
		gl_panelGameMode.setHorizontalGroup(
				gl_panelGameMode.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelGameMode.createSequentialGroup()
						.addContainerGap()
						.addComponent(panelFirstMove, GroupLayout.PREFERRED_SIZE, 237, Short.MAX_VALUE)
						.addGap(28))
						.addGroup(gl_panelGameMode.createSequentialGroup()
								.addContainerGap()
								.addComponent(panelLevel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGap(28))
								.addGroup(gl_panelGameMode.createSequentialGroup()
										.addGap(46)
										.addGroup(gl_panelGameMode.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panelGameMode.createSequentialGroup()
														.addComponent(buttonStart, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
														.addGap(47))
														.addGroup(gl_panelGameMode.createSequentialGroup()
																.addComponent(radioButtonShowOrder, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addGap(39))))
				);
		gl_panelGameMode.setVerticalGroup(
				gl_panelGameMode.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelGameMode.createSequentialGroup()
						.addGap(50)
						.addComponent(panelFirstMove, GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
						.addGap(104)
						.addComponent(panelLevel, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
						.addGap(32)
						.addComponent(buttonStart, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
						.addGap(30)
						.addComponent(radioButtonShowOrder, GroupLayout.PREFERRED_SIZE, 37, Short.MAX_VALUE)
						.addContainerGap())
				);
		panelGameMode.setLayout(gl_panelGameMode);
		contentPane.add(panelWindowOperate);

		buttonBack = new MyJButton(3);
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindowUI.getMainFrame().setVisible(true);
			}
		});
		buttonBack.setText("BACK");
		buttonBack.setFont(new Font("Yu Gothic UI", Font.PLAIN, 19));
		GroupLayout gl_panelWindowOperate = new GroupLayout(panelWindowOperate);
		gl_panelWindowOperate.setHorizontalGroup(
				gl_panelWindowOperate.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelWindowOperate.createSequentialGroup()
						.addContainerGap()
						.addComponent(buttonBack, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(buttonMin, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(buttonMax, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(buttonExit, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
				);
		gl_panelWindowOperate.setVerticalGroup(
				gl_panelWindowOperate.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelWindowOperate.createParallelGroup(Alignment.BASELINE)
						.addComponent(buttonBack, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonExit, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonMin, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonMax, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
				);
		panelWindowOperate.setLayout(gl_panelWindowOperate);
		contentPane.add(labelIcon);
		if (MainWindowUI.getGameMode() == 3) {
			panelGameMode.setVisible(false);
		}

		if (panelTable.getScreenModel() == ScreenModel.MIN) {
			windowWidth = 1140;
			windowHeight = 750;
			windowStartX = screenWidth/2 - windowWidth/2;
			windowStartY = screenHeight/2 - windowHeight/2 - 15;
			setBounds(windowStartX, windowStartY, windowWidth, windowHeight);
		}
		initializeWindow();
//		panelTable.updateChessLoop(MainWindowUI.getGameMode());
	}

	private void initializeWindow() {
		if (panelTable.getScreenModel() == ScreenModel.MIN) {
			chessTableSize = 650;
			panelTable.setBounds(getWidth()/2 - (int)(panelTable.getWidth()*0.5), 
					0, chessTableSize, chessTableSize);

			buttonRetract.setPreferredSize(new Dimension(40, 10));
			buttonRetract.setFont(new Font("楷体", Font.PLAIN, 25));
			buttonPlayAgain.setPreferredSize(new Dimension(40, 10));
			buttonPlayAgain.setFont(new Font("楷体", Font.PLAIN, 25));

			panelPlayChessOperate.setBounds(getWidth()/2 - 300, getHeight() - 100, 500, 68);
			panelWindowOperate.setBounds( (int)(getWidth()*0.66), 4, 373, 46);
			panelGameMode.setBounds(850, 0, 270, 690);
		}
	}

	public void windowControl(int size) {  //1为小窗口，2为全屏
		int windowhalfWidth = getWidth()/2;
		int windowhalfHeigth = getHeight()/2;

		panelMain.setBounds(0, 58, screenWidth, screenHeight - 50);
		if (panelTable.getScreenModel() == ScreenModel.MAX) {
			panelWindowOperate.setBounds(getWidth() - 376, 4, 373, 46);
			panelTable.setBounds(windowhalfWidth - 400, windowhalfHeigth - (int)(getHeight()*0.474), chessTableSize, chessTableSize);
			if (size == 1) {
				panelPlayChessOperate.setBounds(getWidth()/2 - 315, (int)(getHeight() * 0.85), 629, 68);
				panelGameMode.setBounds(1183, 83, 267, 800);
			} else {
				panelPlayChessOperate.setBounds(getWidth()/2 - 315, (int)(getHeight() * 0.83), 629, 68);
				panelGameMode.setBounds(windowhalfWidth + 550, windowhalfHeigth - 500, 267, 800);
			}
		} else {
			panelWindowOperate.setBounds(getWidth() - 376, 4, 373, 46);
			if (size == 1) {
				panelPlayChessOperate.setBounds(getWidth()/2 - 300, getHeight() - 100, 500, 60);
				panelGameMode.setBounds(850, 20, 270, 690);
				panelTable.setBounds(getWidth()/2 - (int)(getHeight()*0.5), 
						0, chessTableSize, chessTableSize);
			} else {
				panelPlayChessOperate.setBounds(getWidth()/2 - 250, getHeight() - 300, 500, 68);
				panelTable.setBounds(getWidth()/2 - panelTable.getWidth()/2, 
						(int)(getHeight()*0.1), chessTableSize, chessTableSize);
				panelGameMode.setBounds(screenWidth - 600, 50, 267, 800);
			}
		}
	}

	public void palyChessSound() {
		try {  
			FileInputStream filedown = new  FileInputStream("/sounds/playChess.wav");
			AudioStream asdown = new AudioStream(filedown);
			AudioPlayer.player.start(asdown);
		} catch (Exception e) {  
			e.printStackTrace();  
		}
	}

	public void rePlay() {
		isCanPlay = false;
		initializeChessData();
		isPlaying = true;
		if (MainWindowUI.getGameMode() == 1 && computerRole == 1) {
			computerPlay();
		} else {
			isCanPlay = true;
		}
	}

	public void computerPlay() {
		
//		RedirectOutPrint();
		isCanPlay = false;
		long before = System.currentTimeMillis();
		GoBangAI goBangAI = new GoBangAI(maxDepth, chessboard);
//		if (System.currentTimeMillis() - before < 400) {
//			try {
//				Thread.sleep(300);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
		isCanPlay = true;
		if (isCanPlay && isPlaying) {
			ChessPoint chessPoint = goBangAI.getOptimumChessPoint();
			chessboard[chessPoint.getX()][chessPoint.getY()] = nowPlayer;
			chessStack.push(chessPoint);
			nowPlayer = 3 - nowPlayer;  //转化角色
			new MusicPlay(2);
			panelTable.updateChessOnce(MainWindowUI.getGameMode());
			count++;
		}
	}

	private void deleteFile(File file){
		File[] files = file.listFiles();
		for(int i=0; i<files.length; i++){
			files[i].delete();
		}
	}
	
	private void RedirectOutPrint() {
		PrintStream ps;
		try {
			String oder = String.valueOf(count + 1);
			String bufferPath = MainWindowUI.getBufferFolder() + "AIdebug\\";
			File bufferdir = new File(bufferPath);
			if (!bufferdir.exists()) {
				bufferdir.mkdirs();
			}
			MainWindowUI.getLocalPlayFrame().deleteFile(bufferdir);
			ps = new PrintStream(new FileOutputStream(bufferPath + oder + ".txt"));
			System.setOut(ps);
			System.setErr(ps);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void initializeChessData() {
		count = 0;
		chessStack.clear();
		chessboard = new int[16][16];
		panelTable.updateUI();
		nowPlayer = 1;
		panelTable.setWin(false);
		regretOpportunity = 0;
	}
}

