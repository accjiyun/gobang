﻿package com.gobang.gobangui;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.ListCellRenderer;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JLayeredPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import com.gobang.netmode.clientdata.ChessClient;
import com.gobang.netmode.util.ClientDataClass;
import com.gobang.util.chessdata.ChessPoint;
import com.gobang.util.uipattern.DesktopCapture;
import com.gobang.util.uipattern.FaceWindow;
import com.gobang.util.uipattern.MusicPlay;
import com.gobang.util.uipattern.MyDialog;
import com.gobang.util.uipattern.MyJButton;
import com.gobang.util.uipattern.MyTableJPanel;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 网络五子棋对战
 *
 */
public class PlayFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private static int[][] Chessboard  = new int[16][16]; //棋盘数组
	private static int nowPlayer; //nowPlayer = 1为黑色,2为白色,-1为平局
	private static double mouseOnTableX, mouseOnTableY; 
	private static int pointOnTableX, pointOnTableY;
	public static Stack<ChessPoint> chessStack = new Stack<ChessPoint>(); //棋子堆栈
	private static int count = 0;  //回合数

	private static boolean isCanPlay = false;//能否下棋状态
	private static boolean isSendSecretMassage = false;  //私聊的状态
	private static Vector<ClientDataClass> secretTargetsClientData = new Vector<ClientDataClass>(20); //私聊对象

	private static int userNumber;
	private static Vector<Object> onlines;

	private static AbstractListModel<Object> listModel;
	private static int mouseOnSceenX, mouseOnSceenY;
	private static int jframeX, jframeY;

	final int screenWidth = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().width);
	final int screenHeight = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height);
	int windowStartX = screenWidth/2 - 685;
	int windowStartY = screenHeight/2 - 520;
	int windowWidth = 1370;
	int windowHeight = 978;

	private static Vector<String> picVector = new Vector<String>();
	/**
	 *   组件的声明
	 */
	private FaceWindow faceWindow = null;

	private JButton buttonSendPic;
	private static MyJButton ButtonStartGame;
	private static MyJButton buttonKickUser;
	private static MyJButton ButtonApplyBattle;
	private MyJButton buttonDefeat;
	private MyJButton buttonRetract;
	private MyJButton buttonCompromise;
	private static JPanel contentPane;
	private static MyTableJPanel panelTable;
	private static JTextPane textAreaChatLog;
	private	static JScrollPane scrollPaneList;
	private	static JTextPane textAreaSendMassage;
	private static JList<Object> userList;
	private static JLayeredPane layeredPane;
	private static JPanel panelAdjustMen;
	private static JMenuItem menuItemStartGame;
	private static JMenuItem menuItemApplyBattle;
	private static JMenuItem menuItemKickUser;
	private JMenuItem menuItemCompromise;
	private JMenuItem menuItemRetract;
	private JMenuItem menuItemDefeat;
	private JMenuItem menuItemSecretChat;
	private JPanel panelOption;
	private static JPopupMenu popupMenu;
	private JTabbedPane tabbedPane;
	private JScrollPane scrollPaneChatLog;
	private JButton ButtonExpression;
	private JPanel panelMain;
	private JPanel panel;
	private JPanel panelChat;
	private JScrollPane scrollPaneSendMassage;
	private JPanel panelList;
	private MyJButton ButtonSendSecretChat;
	private JPanel panelMainTitle;
	private JMenuBar menuBar;
	private JLabel labelIcon;
	private Component horizontalStrutLeft;
	private JLabel labelTitle;
	private Component horizontalStrutRight;
	private JMenu menuOption;
	private JSeparator separator;
	private JMenu menuExit;
	private JMenuItem menuItemExitGame;
	private JMenuItem menuItemExitRoom;
	private JMenuItem menuItemBackMianWindow;
	private JButton buttonScreenshot;
	/**
	 * 一些get() 和 set() 函数
	 * @return
	 */

	public static boolean isCanPlay() {
		return isCanPlay;
	}

	public static int getNowPlayer() {
		return nowPlayer;
	}

	public static void setNowPlayer(int nowPlayer) {
		PlayFrame.nowPlayer = nowPlayer;
	}

	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		PlayFrame.count = count;
	}

	public static MyTableJPanel getPanelTable() {
		return panelTable;
	}

	public static Stack<ChessPoint> getChessStack() {
		return chessStack;
	}

	public static void pushChessStack(ChessPoint chessPoint) {
		if (chessPoint != null){
			Chessboard[chessPoint.getX()][chessPoint.getY()] = chessPoint.getNowPlay();
			chessStack.push(chessPoint);
		} else {
			System.err.println("pushChessStack():chessPoint==null");
		}
	}


	public static void setChessStack(Stack<ChessPoint> chessStack) {
		if (chessStack != null) {
			PlayFrame.chessStack = chessStack;
			Chessboard=new int[16][16];
			for(ChessPoint chessPoint :chessStack){
				Chessboard[chessPoint.getX()][chessPoint.getY()]=chessPoint.getNowPlay();
			}
		}else {
			System.err.println("setChessStack():chessStack==null");
		}
	}

	public static int[][] getChessboard() {
		return Chessboard;
	}

	public static void setCanPlay(boolean isCanPlay) {
		PlayFrame.isCanPlay = isCanPlay;
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PlayFrame() {

		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(windowStartX, windowStartY, windowWidth, windowHeight);
		UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("楷体", Font.BOLD, 30)));
		isCanPlay = false;

		contentPane = new JPanel() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				contentPane.setSize(1370, 976);
				ImageIcon backGround = new ImageIcon(getClass()
						.getResource("/resources/background/background1.png"));
				if (screenHeight < 800) {
					backGround = new ImageIcon(getClass()
							.getResource("/resources/background/background1_MIN.png"));
				}
				g.drawImage(backGround.getImage(), 0, 0, contentPane.getWidth(), 
						contentPane.getHeight(), contentPane);
			}
		};

		contentPane.setOpaque(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		panelMain = new JPanel();
		panelMain.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(jframeX + (arg0.getXOnScreen() - mouseOnSceenX), 
						jframeY + (arg0.getYOnScreen() - mouseOnSceenY));
			}
		});
		panelMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				mouseOnSceenX = arg0.getXOnScreen();
				mouseOnSceenY = arg0.getYOnScreen();
				jframeX = getX();
				jframeY = getY();
			}
		});
		panelMain.setFont(new Font("楷体", Font.BOLD, 28));
		panelMain.setOpaque(false);
		panelMain.setBackground(Color.WHITE);
		contentPane.add(panelMain);
		panelMain.setLayout(null);

		panelTable = new MyTableJPanel();
		panelTable.setOpaque(false);

		/**
		 * 鼠标点击下棋
		 */
		panelTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Thread() {
					public void run() {
						if (ChessClient.getIsBegin() == false || isCanPlay == false) {
							return;
						}
						mouseOnTableX = panelTable.getMousePosition().getX();
						mouseOnTableY = panelTable.getMousePosition().getY();

						pointOnTableX = panelTable.coDispose(mouseOnTableX - panelTable.getMouseOffset());
						pointOnTableY = panelTable.coDispose(mouseOnTableY - panelTable.getMouseOffset());
						ChessPoint chessPoint = new ChessPoint(pointOnTableX, pointOnTableY, nowPlayer);
						if (pointOnTableX <= 15 && pointOnTableX > 0
								&& pointOnTableY <= 15 && pointOnTableY > 0
								&& Chessboard[pointOnTableX][pointOnTableY] == 0
								&& isCanPlay && nowPlayer <= 2 && ChessClient.getIsBegin()) {

							count++;
							chessStack.push(chessPoint);
							Chessboard[pointOnTableX][pointOnTableY] = nowPlayer;
							if (ChessClient.SendServerChess(chessPoint, count)) {
								isCanPlay = false;
								new MusicPlay(2);
								showAllChess();
							} else {
								System.err.println("ChessClient.SendServerChess()--send fail!!");
							}
						}
					}
				}.start();
			}
		});

		panelTable.setBounds(33, 26, 800, 800);
		panelMain.add(panelTable);

		panelOption = new JPanel();
		panelOption.setOpaque(false);
		panelOption.setBounds(33, 838, 800, 78);
		panelMain.add(panelOption);
		panelOption.setLayout(null);
		/**
		 * 认输按钮
		 */
		buttonDefeat = new MyJButton();
		buttonDefeat.setText("认输");
		buttonDefeat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (ChessClient.getIsBegin() == false) {
					MyDialog myDialog = new MyDialog();
					myDialog.showMessageDialog("提示", "游戏还未开始！", 1);
					return;
				}
				isCanPlay = false;
				if (!ChessClient.giveUp()){
					System.err.println("ChessClient.giveUp()--send fail!!");
				}
			}
		});
		buttonDefeat.setFont(new Font("楷体", Font.PLAIN, 36));
		buttonDefeat.setBounds(14, 7, 187, 71);
		panelOption.add(buttonDefeat);

		/**
		 * 悔棋按钮
		 */
		buttonRetract = new MyJButton();
		buttonRetract.setText("悔棋");
		buttonRetract.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MyDialog myDialog = new MyDialog();
				if (ChessClient.getIsBegin() == false) {
					myDialog.showMessageDialog("提示", "游戏还未开始！", 1);
					return;
				}
				if (chessStack.isEmpty() || count < 2) {
					myDialog.showMessageDialog("提示", "才几步呢，你就悔棋？\n要不得!", 1);
					return;
				}
				if (!ChessClient.askForBack()){
					System.err.println("ChessClient.askForBack()--send fail!!");
					return;
				}
				isCanPlay = false;
			}
		});
		buttonRetract.setFont(new Font("楷体", Font.PLAIN, 36));
		buttonRetract.setBounds(297, 7, 187, 71);
		panelOption.add(buttonRetract);

		/**
		 * 求和按钮
		 */
		buttonCompromise = new MyJButton();
		buttonCompromise.setText("求和");
		buttonCompromise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyDialog myDialog = new MyDialog();
				if (ChessClient.getIsBegin() == false) {
					myDialog.showMessageDialog("提示", "游戏还未开始！", 1);
					return;
				}
				if (!ChessClient.askForPeace()) {
					System.err.println("ChessClient.askForPeace()--send fail!!");
				}
				isCanPlay = false;
			}
		});
		buttonCompromise.setFont(new Font("楷体", Font.PLAIN, 36));
		buttonCompromise.setBounds(579, 7, 187, 71);
		panelOption.add(buttonCompromise);

		panel = new JPanel();
		panel.setOpaque(false);
		panel.setBounds(847, 26, 499, 903);
		panelMain.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		UIManager.put("TabbedPane.contentOpaque", false);
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setName("");
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				showAllChess();
				setUpData();
			}
		});
		tabbedPane.setFont(new Font("新宋体", Font.PLAIN, 24));
		tabbedPane.setBackground(new Color(155, 164, 179, 120));
		panel.add(tabbedPane);

		panelChat = new JPanel();
		panelChat.setOpaque(false);
		tabbedPane.addTab("      聊天       ", null, panelChat, null);
		panelChat.setLayout(null);

		scrollPaneChatLog = new JScrollPane(textAreaChatLog,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		//分别设置水平和垂直滚动条自动出现 
		scrollPaneChatLog.setHorizontalScrollBarPolicy( 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		scrollPaneChatLog.setVerticalScrollBarPolicy(
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); 

		scrollPaneChatLog.setOpaque(false);
		scrollPaneChatLog.getViewport().setOpaque(false);
		scrollPaneChatLog.setBounds(14, 13, 466, 678);
		panelChat.add(scrollPaneChatLog);

		textAreaChatLog = new JTextPane();
		textAreaChatLog.setEditable(false);
		textAreaChatLog.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				scrollPaneChatLog.repaint();
			}
		});
		//		Timer timerTextAreaChatLog = new Timer();
		//		timerTextAreaChatLog.schedule(new TimerTask() {
		//			public void run() {
		//				scrollPaneChatLog.repaint();
		//			}
		//		}, 0, 140);
		textAreaChatLog.setFont(new Font("楷体", Font.PLAIN, 24));
		textAreaChatLog.setBackground(new Color(255, 255, 255));
		scrollPaneChatLog.setViewportView(textAreaChatLog);

		scrollPaneSendMassage = new JScrollPane(
				textAreaSendMassage,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		//分别设置水平和垂直滚动条自动出现 
		scrollPaneSendMassage.setHorizontalScrollBarPolicy( 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		scrollPaneSendMassage.setVerticalScrollBarPolicy( 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); 

		scrollPaneSendMassage.setOpaque(false);
		scrollPaneSendMassage.getViewport().setOpaque(false);
		scrollPaneSendMassage.setBounds(14, 726, 466, 132);
		panelChat.add(scrollPaneSendMassage);

		textAreaSendMassage = new JTextPane();
		textAreaSendMassage.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER 
						&& e.isControlDown())  { //按回车键执行相应操作; 
					SendChatMassage();
				}
			}
		});
		textAreaSendMassage.setFont(new Font("楷体", Font.PLAIN, 22));
		textAreaSendMassage.setBackground(new Color(255, 255, 255));
		scrollPaneSendMassage.setViewportView(textAreaSendMassage);

		ButtonExpression = new JButton(new ImageIcon(getClass()
				.getResource("/resources/ico/expression.png")));
		ButtonExpression.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if (null == faceWindow) {
					faceWindow = new FaceWindow(PlayFrame.this);
					faceWindow.setVisible(true);
					// 设置控件相对于父窗体的位置
					Point localPosition = getLocationOnScreen();
					faceWindow.setBounds(localPosition.x + 860, localPosition.y + 495, 475, 300);
				}
				faceWindow.requestFocus();
			}
		});
		ButtonExpression.setContentAreaFilled(false);
		//		ButtonExpression.setFocusPainted(false);   //是否绘制焦点
		ButtonExpression.setBorderPainted(false);	//是否画边框
		//		ButtonExpression.setPreferredSize(new Dimension(35,35));
		ButtonExpression.setMargin(new Insets(0, 0, 0, 0)); //边距改为0
		ButtonExpression.setPressedIcon(new ImageIcon(getClass()
				.getResource("/resources/ico/expression.png")));
		ButtonExpression.setBounds(14, 692, 35, 35);
		panelChat.add(ButtonExpression);

		buttonSendPic = new JButton(new ImageIcon(getClass()
				.getResource("/resources/ico/injectpic.png")));
		buttonSendPic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = 
						new FileNameExtensionFilter("图片(jpg,png,bmp,gif)","gif","jpg", "png", "bmp");
				chooser.setFileFilter(filter);
				chooser.showOpenDialog(null);
				if (chooser.getFileFilter() != null) {
					String soucePath = chooser.getSelectedFile().toString();
					String savePath = saveImageToTemp(soucePath);
					insertIconOnSendPanel(savePath);
					textAreaSendMassage.requestFocus();
					if (!ChessClient.sendImageToTemp(savePath)){
						System.err.println("ChessClient.sendImageToTemp--send fail!!");
					}
				}
			}
		});
		buttonSendPic.setMargin(new Insets(0, 0, 0, 0));
		buttonSendPic.setContentAreaFilled(false);
		buttonSendPic.setBorderPainted(false);
		buttonSendPic.setBounds(63, 692, 35, 35);
		panelChat.add(buttonSendPic);

		buttonScreenshot = new JButton(new ImageIcon(getClass()
				.getResource("/resources/ico/screenshot.png")));
		buttonScreenshot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				new Thread() {
					public void run() {
						DesktopCapture desk = new DesktopCapture();
						while (desk.getCaptureImg() == null) {
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						String imagePath = desk.getCaptureImgPath();
						if (imagePath != null) {
							if (!ChessClient.sendImageToTemp(imagePath)){
								System.err.println("ChessClient.sendImageToTemp--send fail!!");
								return;
							}
							textAreaSendMassage.insertIcon(new ImageIcon(imagePath));
						}
						textAreaSendMassage.setCaretPosition(
								textAreaSendMassage.getStyledDocument().getLength());
						textAreaSendMassage.requestFocus();
					}
				}.start();
			}
		});
		buttonScreenshot.setMargin(new Insets(0, 0, 0, 0));
		buttonScreenshot.setContentAreaFilled(false);
		buttonScreenshot.setBorderPainted(false);
		buttonScreenshot.setBounds(112, 692, 35, 35);
		panelChat.add(buttonScreenshot);

		panelList = new JPanel();
		panelList.setOpaque(false);
		tabbedPane.addTab("     成员列表       ", null, panelList, null);
		panelList.setLayout(null);

		scrollPaneList = new JScrollPane();
		scrollPaneList.setBackground(new Color(255, 255, 255));
		scrollPaneList.getViewport().setOpaque(false);
		scrollPaneList.setBounds(14, 13, 466, 744);
		panelList.add(scrollPaneList);

		onlines = new Vector<Object>();
		listModel = new UUListModel(onlines);
		userList = new JList(listModel);
		userList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int index = userList.locationToIndex(e.getPoint());
				userList.setSelectedIndex(index);
			}
		});

		userList.setFont(new Font("等线", Font.PLAIN, 31));
		userList.setCellRenderer(new CellRenderer());
		userList.setOpaque(false);
		Border etch = BorderFactory.createEtchedBorder();
		userList.setBorder(BorderFactory.createTitledBorder(etch, "在线客户数: "      //显示房间人数及列表
				+ "<"+userNumber+">", TitledBorder.LEADING, TitledBorder.TOP, new Font(
						"sdf", Font.BOLD, 20), new Color(108, 208, 196)));
		scrollPaneList.setViewportView(userList);

		popupMenu = new JPopupMenu();
		popupMenu.setFont(new Font("楷体", Font.PLAIN, 35));
		addPopup(userList, popupMenu);

		menuItemSecretChat = new JMenuItem(" 悄悄话");
		menuItemSecretChat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applySecretChat();
			}
		});
		menuItemSecretChat.setFont(new Font("楷体", Font.PLAIN, 24));
		popupMenu.add(menuItemSecretChat);

		menuItemKickUser = new JMenuItem("  踢人   ");
		menuItemKickUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				MyDialog myDialog = new MyDialog();
				if (userList.getSelectedIndex() == 0) {
					myDialog.showMessageDialog("提示", "自己就别踢了吧！", 1);
					return;
				}
				if (!ChessClient.kickClient(ChessClient.clientdataVec.get(userList.getSelectedIndex()))){
					System.err.println("ChessClient.kickClient()--send fall!!");
				}
			}
		});
		menuItemKickUser.setFont(new Font("楷体", Font.PLAIN, 24));
		popupMenu.add(menuItemKickUser);

		panelAdjustMen = new JPanel();
		panelAdjustMen.setOpaque(false);
		panelAdjustMen.setBounds(14, 770, 466, 77);
		panelList.add(panelAdjustMen);

		ButtonSendSecretChat = new MyJButton();
		ButtonSendSecretChat.setBounds(303, 13, 149, 64);
		ButtonSendSecretChat.setText("悄悄话");
		ButtonSendSecretChat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applySecretChat();
			}                                                                                                                                                                       
		});
		panelAdjustMen.setLayout(null);
		ButtonSendSecretChat.setFont(new Font("楷体", Font.PLAIN, 25));
		panelAdjustMen.add(ButtonSendSecretChat);

		layeredPane = new JLayeredPane();
		layeredPane.setBounds(14, 13, 158, 64);

		panelAdjustMen.add(layeredPane);
		buttonKickUser = new MyJButton();
		buttonKickUser.setText("踢人");
		panelAdjustMen.add(buttonKickUser);
		buttonKickUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selectedNumber = userList.getSelectedIndices();
				MyDialog myDialog = new MyDialog();
				if (selectedNumber.length < 1) {
					myDialog.showMessageDialog("提示", "请选择要踢的对象", 1);
					return;
				}
				for (int i = 0; i < selectedNumber.length; i++) {	
					if (ChessClient.getClientDataClass().equals(
							ChessClient.clientdataVec.get(selectedNumber[i]) )) {
						myDialog.showMessageDialog("提示", "自己就别踢了吧！", 1);
						continue;
					}
					if (!ChessClient.kickClient(ChessClient.clientdataVec.get(selectedNumber[i]))){
						System.err.println("ChessClient.kickClient()--send fall!!");
					}
				}
				updateUserList();
			}
		});

		buttonKickUser.setBounds(183, 12, 104, 64);
		buttonKickUser.setFont(new Font("楷体", Font.PLAIN, 26));


		ButtonStartGame = new MyJButton();
		ButtonStartGame.setBounds(0, 0, 157, 61);
		layeredPane.add(ButtonStartGame); 
		ButtonStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (ChessClient.getIsBegin()) {
					MyDialog myDialog = new MyDialog();
					myDialog.showMessageDialog("提示", "比赛已经开始！", 1);
					return;
				}
				startGame();
			}
		});
		ButtonStartGame.setFont(new Font("楷体", Font.PLAIN, 26));
		ButtonStartGame.setText("开始游戏");		

		ButtonApplyBattle = new MyJButton();   
		ButtonApplyBattle.setBounds(0, 0, 157, 61);
		ButtonApplyBattle.setText("申请对战");
		ButtonApplyBattle.setFont(new Font("楷体", Font.PLAIN, 27));
		layeredPane.add(ButtonApplyBattle);
		ButtonApplyBattle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyDialog myDialog = new MyDialog();
				if (ChessClient.getIsBegin() 
						&& ChessClient.clientdataVec.get(1).getClientName().equals(
								ChessClient.getClientName())) {
					myDialog.showMessageDialog("提示", "比赛已经开始！", 1);
					return;
				}
				if (!ChessClient.applyFight()){
					System.err.println("ChessClient.applyFight()--send fall!!");
					return;
				}
				updateUserList();
				if (ButtonApplyBattle.getText().equals("申请对战")) {
					ButtonApplyBattle.setText("取消准备");
				} else {
					ButtonApplyBattle.setText("申请对战");
				}
			}
		});

		panelMainTitle = new JPanel();
		panelMainTitle.setBackground(Color.WHITE);
		contentPane.add(panelMainTitle, BorderLayout.NORTH);
		panelMainTitle.setLayout(new BorderLayout(0, 0));

		menuBar = new JMenuBar();
		menuBar.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				setLocation(jframeX + (e.getXOnScreen() - mouseOnSceenX), jframeY 
						+ (e.getYOnScreen() - mouseOnSceenY));
			}
		});
		menuBar.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mouseOnSceenX = e.getXOnScreen();
				mouseOnSceenY = e.getYOnScreen();
				jframeX = getX();
				jframeY = getY();
			}
		});
		panelMainTitle.add(menuBar);

		labelIcon = new JLabel("");
		labelIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panelTable.setShowOrder(!panelTable.isShowOrder());
				showAllChess();
			}
		});
		labelIcon.setIcon(new ImageIcon(getClass().getResource("/resources/ico/GoBangIco.png")));
		menuBar.add(labelIcon);

		int leftWidth = 553, rightWidth = 525;
		if (panelTable.getScreenModel() == ScreenModel.MIN) {
			leftWidth = 470;
			rightWidth = 380;
		}

		horizontalStrutLeft = Box.createHorizontalStrut(leftWidth);
		menuBar.add(horizontalStrutLeft);

		labelTitle = new JLabel("\u4E94\u5B50\u68CB\u5BF9\u6218");
		labelTitle.setFont(new Font("楷体", Font.BOLD, 28));
		menuBar.add(labelTitle);

		horizontalStrutRight = Box.createHorizontalStrut(rightWidth);
		menuBar.add(horizontalStrutRight);

		menuOption = new JMenu("选项");
		menuOption.setFont(new Font("新宋体", Font.PLAIN, 18));
		menuOption.setHorizontalAlignment(SwingConstants.RIGHT);
		menuBar.add(menuOption);

		menuItemStartGame = new JMenuItem("开始游戏");
		menuItemStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyDialog myDialog = new MyDialog();
				if (ChessClient.getIsBegin()) {
					myDialog.showMessageDialog("提示", "比赛已经开始！", 1);
					return;
				}
				startGame();
			}
		});
		menuItemStartGame.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuOption.add(menuItemStartGame);

		menuItemApplyBattle = new JMenuItem("申请对战");
		menuItemApplyBattle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!ChessClient.applyFight()){
					System.err.println("ChessClient.applyFight()--send fall!!");
					return;
				}
				updateUserList();
			}
		});
		menuItemApplyBattle.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuOption.add(menuItemApplyBattle);

		separator = new JSeparator();
		menuOption.add(separator);

		menuItemDefeat = new JMenuItem("\u8BA4\u8F93");
		menuItemDefeat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!ChessClient.giveUp()){
					System.err.println("ChessClient.giveUp()--send fall!!");
					return;
				}
				isCanPlay = false;
			}
		});
		menuItemDefeat.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuOption.add(menuItemDefeat);

		menuItemRetract = new JMenuItem("悔棋");
		menuItemRetract.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyDialog myDialog = new MyDialog();
				if (chessStack.isEmpty() || count < 2) {
					myDialog.showMessageDialog("提示", "才几步呢，你就悔棋？\n要不得!", 1);
					return;
				}
				if (!ChessClient.askForBack()){
					System.err.println("ChessClient.askForBack()--send fall!!");
				}
				isCanPlay = false;
			}
		});
		menuItemRetract.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuOption.add(menuItemRetract);

		menuItemCompromise = new JMenuItem("求和");
		menuItemCompromise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!ChessClient.askForPeace()) {
					System.err.println("ChessClient.askForPeace()--send fall!!");
				}
				isCanPlay = false;
			}
		});
		menuItemCompromise.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuOption.add(menuItemCompromise);

		menuExit = new JMenu("退出");
		menuExit.setFont(new Font("新宋体", Font.PLAIN, 18));
		menuBar.add(menuExit);

		menuItemExitGame = new JMenuItem("退出游戏");
		menuItemExitGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!ChessClient.exitGame()) {
					System.err.println("ChessClient.exitGame()--send fall!!");
				}
			}
		});
		menuItemExitGame.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuExit.add(menuItemExitGame);

		menuItemExitRoom = new JMenuItem("\u9000\u51FA\u623F\u95F4");
		menuItemExitRoom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!ChessClient.exitRoom()) {
					System.err.println("ChessClient.exitRoom()--send fall!!");
				}
			}
		});
		menuItemExitRoom.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuExit.add(menuItemExitRoom);

		menuItemBackMianWindow = new JMenuItem("\u56DE\u5230\u4E3B\u754C\u9762");
		menuItemBackMianWindow.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemBackMianWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!ChessClient.goBackMainFrame()) {
					System.err.println("ChessClient.goBackMainFrame()--send fall!!");
				}
			}
		});
		menuExit.add(menuItemBackMianWindow);
		setUpData();
		if (panelTable.getScreenModel() == ScreenModel.MIN) {
			initializeSmallWindow();
		}
	}

	private void initializeSmallWindow() {
		windowWidth = 1140;
		windowHeight = 760;
		windowStartX = screenWidth/2 - windowWidth/2;
		windowStartY = screenHeight/2 - windowHeight/2 - 15;
		setBounds(windowStartX, windowStartY, windowWidth, windowHeight);
		tabbedPane.addTab("      聊天         ", null, panelChat, null);
		tabbedPane.addTab("     成员列表       ", null, panelList, null);
		panelTable.setBounds(20, 10, 650, 650);
		panelMain.setFont(new Font("楷体", Font.BOLD, 28));
		panelOption.setBounds(20, 665, 650, 53);
		buttonDefeat.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonDefeat.setBounds(68, 5, 125, 35);
		buttonRetract.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonRetract.setBounds(263, 7, 125, 35);
		buttonCompromise.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonCompromise.setBounds(455, 5, 125, 35);
		panel.setBounds(690, 10, 440, 690);
		tabbedPane.setFont(new Font("新宋体", Font.PLAIN, 20));
		tabbedPane.setBackground(new Color(155, 164, 179, 120));
		scrollPaneChatLog.setBounds(14, 13, 400, 531);
		textAreaChatLog.setFont(new Font("楷体", Font.PLAIN, 24));
		textAreaChatLog.setBackground(new Color(255, 255, 255, 240));
		scrollPaneSendMassage.setBounds(14, 579, 400, 71);
		textAreaSendMassage.setFont(new Font("楷体", Font.PLAIN, 24));
		textAreaSendMassage.setBackground(new Color(255, 255, 255, 240));
		scrollPaneList.setBackground(new Color(255, 255, 255, 240));
		scrollPaneList.setBounds(14, 13, 400, 570);
		userList.setFont(new Font("等线", Font.PLAIN, 31));
		popupMenu.setFont(new Font("楷体", Font.PLAIN, 35));
		menuItemSecretChat.setFont(new Font("楷体", Font.PLAIN, 24));
		menuItemKickUser.setFont(new Font("楷体", Font.PLAIN, 24));
		panelAdjustMen.setBounds(14, 600, 400, 50);
		layeredPane.setBounds(15, 7, 120, 35);
		ButtonStartGame.setFont(new Font("楷体", Font.PLAIN, 20));
		ButtonStartGame.setBounds(0, 0, 120, 35);
		ButtonApplyBattle.setBounds(0, 0, 120, 35);
		ButtonApplyBattle.setFont(new Font("楷体", Font.PLAIN, 20));
		buttonKickUser.setBounds(167, 7, 80, 35);
		buttonKickUser.setFont(new Font("楷体", Font.PLAIN, 20));
		ButtonSendSecretChat.setBounds(279, 7, 105, 35);
		ButtonSendSecretChat.setFont(new Font("楷体", Font.PLAIN, 20));
		ButtonExpression.setBounds(14, 546, 35, 35);
		buttonSendPic.setBounds(50, 546, 35, 35);
		buttonScreenshot.setBounds(86, 546, 35, 35);
		labelTitle.setFont(new Font("楷体", Font.BOLD, 28));
		menuOption.setFont(new Font("新宋体", Font.PLAIN, 18));
		menuItemStartGame.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemApplyBattle.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemDefeat.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemRetract.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemCompromise.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuExit.setFont(new Font("新宋体", Font.PLAIN, 18));
		menuItemExitGame.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemExitRoom.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		menuItemBackMianWindow.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
	}

	public void showNoticeIntable(String Message, String PlayerName) {

		java.text.DateFormat dataFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String headMassages = PlayerName + "  " + dataFormat.format(new Date());
		String textMassages = "\n   " + Message + "\n";
		StyledDocument textAreaChatLogStyledDoc = textAreaChatLog.getStyledDocument();
		Style styleHead = textAreaChatLogStyledDoc.addStyle(null, null);
		Style styleText = textAreaChatLogStyledDoc.addStyle(null, null);

		StyleConstants.setFontFamily(styleHead, "新宋体");
		StyleConstants.setFontFamily(styleText, "微软雅黑");
		StyleConstants.setFontSize(styleHead, 16);
		StyleConstants.setFontSize(styleText, 18);
		StyleConstants.setForeground(styleHead, new Color(227, 77, 45));
		StyleConstants.setForeground(styleText, new Color(227, 77, 45));

		try {
			int offset = textAreaChatLogStyledDoc.getLength();
			textAreaChatLogStyledDoc.insertString(offset, headMassages, styleHead);
			offset = textAreaChatLogStyledDoc.getLength();
			textAreaChatLogStyledDoc.insertString(offset, textMassages, styleText);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textAreaChatLog.setCaretPosition(textAreaChatLogStyledDoc.getLength());//设置滚动到最下边
	}

	public void showChatIntable(StyledDocument sendDoc, String PlayerName){

		java.text.DateFormat dataFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String headMassages = PlayerName + "  " + dataFormat.format(new Date());
		Style styleHead = textAreaChatLog.getStyledDocument().addStyle(null, null);
		Style styleText = textAreaChatLog.getStyledDocument().addStyle(null, null);

		StyleConstants.setFontFamily(styleHead, "新宋体");
		StyleConstants.setFontFamily(styleText, "楷体");
		StyleConstants.setFontSize(styleHead, 16);
		StyleConstants.setFontSize(styleText, 22);
		StyleConstants.setForeground(styleHead, Color.blue);
		if (PlayerName.equals(ChessClient.getClientName())) {
			StyleConstants.setForeground(styleHead, new Color(126, 199, 37));
		}

		try {
			//消息头部
			textAreaChatLog.getStyledDocument().insertString(
					textAreaChatLog.getStyledDocument().getLength(), headMassages, styleHead);
			textAreaChatLog.getStyledDocument().insertString(
					textAreaChatLog.getStyledDocument().getLength(), "\n   ", styleText);

			for(int i = 0; i < sendDoc.getRootElements()[0].getElement(0).getElementCount(); i++){
				Icon icon = StyleConstants.getIcon(
						sendDoc.getRootElements()[0].getElement(0).getElement(i).getAttributes());
				if(icon != null){
					File tempFile = new File(icon.toString().trim());
					String fileName  = tempFile.getName();
					int typeImage = Integer.parseInt(fileName.substring(0, fileName.length()-4));
					String imagePath;
					if (typeImage > 200) {
						imagePath = MainWindowUI.getBufferFolder() + fileName;
					} else {
//						File tempfFile =  new File(getClass().getResource("/resources/expression/" + fileName).getPath());
						imagePath = String.valueOf(typeImage);
//						textAreaChatLog.getStyledDocument().insertString(
//								textAreaChatLog.getStyledDocument().getLength(), imagePath, styleText);
					}
					picVector.add(imagePath);
				}
			}
			int k = 0;
			for(int i = 0; i < sendDoc.getLength(); i++){
				if(sendDoc.getCharacterElement(i).getName().equals("icon")){
					
					if (picVector.get(k).length() < 10) {
						String imagePath = "/resources/expression/" + picVector.get(k++) + ".gif";
						insertIOnChatLogPanel(new ImageIcon(
								getClass().getResource(imagePath)));
					} else {
						insertIOnChatLogPanel(new ImageIcon(picVector.get(k++)));
					}
				}else{
					try {
						String text = sendDoc.getText(i,1);
						textAreaChatLog.getStyledDocument().insertString(
								textAreaChatLog.getStyledDocument().getLength(), text, styleText);
					} catch (BadLocationException e1) {
						e1.printStackTrace();
					}
				}
			}
			picVector.clear();
			textAreaChatLog.getStyledDocument().insertString(
					textAreaChatLog.getStyledDocument().getLength(), "\n", styleText);

		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	public void SendChatMassage(){

		StyledDocument sendDoc = textAreaSendMassage.getStyledDocument();
		int MessageLen = sendDoc.getLength();
		String playerName = ChessClient.getClientName();
		MyDialog myDialog = new MyDialog();

		if (MessageLen == 0) {
			myDialog.showMessageDialog("提示", "请输入聊天信息！", 1);
			return;
		}
		if(MessageLen > 300){
			myDialog.showMessageDialog("提示", "消息最多300个字符！你要发送的为"
					+ textAreaSendMassage.getStyledDocument().getLength() + "个字符！", 1);
			return;
		}
		showChatIntable(sendDoc, playerName);
		if (isSendSecretMassage) {
			for (int j = 0; j < secretTargetsClientData.size(); j++) {
				ChessClient.secretChat(secretTargetsClientData.get(j), sendDoc);
				isSendSecretMassage = !isSendSecretMassage;
				secretTargetsClientData.clear();
			}
		} else {
			ChessClient.SendServerChat(sendDoc);
		}
		textAreaSendMassage.setText("");
		textAreaSendMassage.setCaretPosition(0);
		textAreaSendMassage.requestFocus();

	}
	
	public void applySecretChat() {
		List<Object> selectedUsers = userList.getSelectedValuesList();
		String myName = ChessClient.getClientName();
		int secretNumber = selectedUsers.size();
		MyDialog myDialog = new MyDialog();
		if (secretNumber < 1) {
			myDialog.showMessageDialog("提示", "请选择私聊对象", 1);
			return;
		}
		if (ChessClient.clientdataVec.get(userList.getSelectedIndex()).getClientName().equals(myName)) {
			myDialog.showMessageDialog("提示", "不能向自己发送信息", 1);
			return;
		}
		tabbedPane.setSelectedIndex(0);
		int offset = textAreaSendMassage.getStyledDocument().getLength();
		Style callStyle = textAreaSendMassage.getStyledDocument().addStyle(null, null);
		StyleConstants.setForeground(callStyle, new Color(95, 197, 222));
		String callFlag = "@" + ChessClient.clientdataVec.get(userList.getSelectedIndex()).getClientName() + ":";
		try {
			textAreaSendMassage.getStyledDocument().insertString(offset, callFlag, callStyle);
			StyleConstants.setForeground(callStyle, Color.BLACK);
			offset = textAreaSendMassage.getStyledDocument().getLength();
			textAreaSendMassage.getStyledDocument().insertString(offset, " ", callStyle);
			textAreaSendMassage.requestFocus();
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		isSendSecretMassage = true;
		int[] selectedNumber = userList.getSelectedIndices();
		for (int i = 0; i < selectedNumber.length; i++) {	
			secretTargetsClientData.add(ChessClient.clientdataVec.get(selectedNumber[i]));
		}
	}

	/**
	 *  插入图片 
	 */
	private void insertIOnChatLogPanel(String IcoPath) {
		textAreaChatLog.setCaretPosition(
				textAreaChatLog.getStyledDocument().getLength());//光标定位到最后
		textAreaChatLog.insertIcon(new ImageIcon(IcoPath)); // 插入图片
	}
	
	public void insertIOnChatLogPanel(Icon icon) {
		textAreaChatLog.setCaretPosition(
				textAreaChatLog.getStyledDocument().getLength()); // 设置插入位置
		textAreaChatLog.insertIcon(icon);
	}
	
	private void insertIconOnSendPanel(String IcoPath) {
		textAreaSendMassage.setCaretPosition(
				textAreaSendMassage.getStyledDocument().getLength());//光标定位到最后
		textAreaSendMassage.insertIcon(new ImageIcon(IcoPath)); // 插入图片
	}

	public void insertImageOnSendPanel(Icon icon) {
		textAreaSendMassage.setCaretPosition(
				textAreaSendMassage.getStyledDocument().getLength()); // 设置插入位置
		textAreaSendMassage.insertIcon(icon);
	}
	
	//复制图片到临时目录
	public String saveImageToTemp(String soucePath){

		int randomNumber = Math.abs((int)(Math.random()*10000000));
		String SuffixName = soucePath.substring(soucePath.lastIndexOf(".")); //提取后缀
		String savePath = MainWindowUI.getBufferFolder() + randomNumber + SuffixName;
		try {
			FileInputStream fis = new FileInputStream(soucePath);
			FileOutputStream fos = new FileOutputStream(savePath);
			byte input[] = new byte[50];
			while (fis.read(input) != -1){
				fos.write(input);
			}
			fis.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return savePath;
	}

	/**
	 * 清空表情面板
	 */
	public void setFaceWindowNull() {
		textAreaSendMassage.requestFocus();
		faceWindow = null;

	}

	/**
	 * 重画所有棋子
	 */
	public static void showAllChess() {
		panelTable.updateChessOnce(MainWindowUI.getGameMode());
	}

	/**
	 * 开始游戏
	 */
	public static void startGame() {
		int selectedIndex = userList.getSelectedIndex();
		MyDialog myDialog = new MyDialog();
		if (selectedIndex == -1 && onlines.size() > 2) {
			myDialog.showMessageDialog("提示", "请选择对象", 1);
			return;
		}
		if ( onlines.size() == 2 ){
			ChessClient.askForBegin();
			return;
		}
		if ( ChessClient.getClientDataClass().equals(ChessClient.clientdataVec.get(selectedIndex)) ) {
			myDialog.showMessageDialog("提示", "不能对自己开战！", 1);
			return;
		}
		ChessClient.askForBegin(ChessClient.clientdataVec.get(selectedIndex));
	}

	/**
	 * 自己请求的悔棋得到同意
	 */
	public void youselfAgreeBack() {
		/*
		 * 如果这边已经下了，就悔两步
		 * 如果没下悔一步
		 */
		int attribute = ChessClient.getAttribute();
		int nowplayer = chessStack.peek().getNowPlay();
		int number;
		if (!chessStack.empty()) {
			if (nowplayer == attribute) {
				count -= 2;
				number = 2;
			} else{			
				count--;
				number = 1;
			}
			while ( number-- > 0) {
				ChessPoint chessPoint = (ChessPoint) chessStack.peek();
				Chessboard[chessPoint.getX()][chessPoint.getY()] = 0;
				chessStack.pop();
			}
			ChessClient.setBegin(true);
			panelTable.setWin(false);
			panelTable.repaint();
		}
	}

	/**
	 * 有人认输了
	 */
	public void otherAgreeBack () {
		ChessClient.setBegin(true);
		panelTable.setWin(false);
		panelTable.repaint();
	}

	public void rePlay() {
		setCount(0);
		if (ChessClient.getAttribute() == 1) {
			setCanPlay(true);
		}
		panelTable.setWin(false);
		chessStack.clear();
		Chessboard = new int[16][16];
		panelTable.repaint();
	}

	/**
	 * 分角色更新按钮
	 */
	public void updateButton() {

		int attribute = ChessClient.getAttribute();
		setNowPlayer(attribute);
		if (attribute == 1) { 
			ButtonStartGame.setVisible(true);     //若为房主，则显示开始游戏，踢人按钮
			buttonKickUser.setVisible(true);
			menuItemKickUser.setVisible(true);
			menuItemStartGame.setVisible(true);
			menuItemApplyBattle.setVisible(false);
			ButtonApplyBattle.setVisible(false);
		}
		else {
			ButtonApplyBattle.setVisible(true);  //若为对手，则显示申请对战按钮
			menuItemApplyBattle.setVisible(true);
			System.out.println("ButtonApplyBattle.setVisible(true); ");
			ButtonStartGame.setVisible(false);
			buttonKickUser.setVisible(false);
			menuItemKickUser.setVisible(false);	
			menuItemStartGame.setVisible(false);
		}
	}
	
	/**
	 * 更新成员列表
	 */
	public void updateUserList() {
		onlines.clear();
		for (int i = 0; i < ChessClient.clientdataVec.size(); i++) {
			String userName = ChessClient.clientdataVec.get(i).getClientName();
			if (ChessClient.clientdataVec.get(i).equals(ChessClient.getClientDataClass())
					&& userName.equals(ChessClient.getClientName())) {
				userName += "(自己)";
			}
			if ( ChessClient.clientdataVec.get(i).getAttribute() == 3) {
				userName += "——准备";
			}
			onlines.add(userName);
		}
		userNumber = onlines.size();
		Border etch = BorderFactory.createEtchedBorder();
		userList.setBorder(BorderFactory.createTitledBorder(etch, "在线客户数: "      //显示房间人数及列表
				+ "<"+userNumber+">", TitledBorder.LEADING, TitledBorder.TOP, new Font(
						"sdf", Font.BOLD, 20), new Color(108, 208, 196)));
		listModel = new UUListModel(onlines);
		userList.setModel(listModel);
		System.out.println("PlayFrame.updateUserList()"+ChessClient.getClientDataClass().toString());
	}

	/**
	 * 在成员列表里添加弹出菜单
	 */
	private void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	/**
	 * 从服务端更新数据
	 */
	void setUpData(){
		ChessClient.upData();
		panelTable.setWin(false);
	}
}

class CellRenderer extends JLabel implements ListCellRenderer<Object> {

	private static final long serialVersionUID = 1L;

	public CellRenderer() {
		setOpaque(true);
	}

	@SuppressWarnings("rawtypes")
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {

		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));// 加入宽度为5的空白边框

		if (value != null) {
			setText(value.toString());
			int indexImage = Math.abs((ChessClient.clientdataVec.get(index).getClientAddress().hashCode() + 
					ChessClient.clientdataVec.get(index).getClientName().hashCode())%19);
			String imagePath = "/resources/head/Character" + String.valueOf(indexImage) + ".jpg";
			setIcon(new ImageIcon(getClass().getResource(imagePath)));
		}
		if (isSelected) {
			setBackground(new Color(255, 255, 153));// 设置背景色
			setForeground(Color.black);
		} else {
			// 设置选取与取消选取的前景与背景颜色.
			setBackground(Color.white); // 设置背景色
			setForeground(Color.black);
		}
		setEnabled(list.isEnabled());
		setFont(new Font("ROMAN_BASELINE", Font.ROMAN_BASELINE, 32));
		setOpaque(true);
		return this;
	}
}

class UUListModel extends AbstractListModel<Object>{

	private static final long serialVersionUID = 1L;
	private Vector<Object> vs;

	public UUListModel(Vector<Object> vs){
		this.vs = vs;
	}

	public Object getElementAt(int index) {
		return vs.get(index);
	}

	public int getSize() {
		return vs.size();
	}

}
