﻿package com.gobang.gobangui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JButton;

import com.gobang.netmode.util.Netmode;
import com.gobang.util.uipattern.MusicPlay;
import com.gobang.util.uipattern.MyJButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainWindowUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private static int gameMode = 1;				//游戏模式： 1.人机对战 2.网络对战    3.人人对战
	private static String ID;//每个程序打开时会自动分配给它一个固定的ID，方便在后面区别于不同的客户使用
	
	final int screenWidth = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().width);
	final int screenHeight = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height);
	int windowStartX;
	int windowStartY;
	private static int mouseOnSceenX, mouseOnSceenY;
	private static int jframeX, jframeY;
	private JPanel contentPane;
	private static MainWindowUI mainFrame;
	private static LocalPlayFrame localPlayFrame;
	private static Netmode netmode = Netmode.LAN;
	private static Object searchJFrame;
	private static boolean isDebug = false;
	private static String bufferFolder = 
			System.getProperty("java.io.tmpdir") + "gobangtemp\\"; //程序临时目录

	MusicPlay startMusic = new MusicPlay(1);				//开始音乐
	
	public static String getBufferFolder() {
		return bufferFolder;
	}
	
	public static boolean isDebug() {
		return isDebug;
	}

	public static void setDebug(boolean isDebug) {
		MainWindowUI.isDebug = isDebug;
	}

	public static String getID() {
		return ID;
	}

	public static Object getSearchframe() {
		return searchJFrame;
	}

	public static LocalPlayFrame getLocalPlayFrame() {
		return localPlayFrame;
	}

	public static MainWindowUI getMainFrame() {
		return mainFrame;
	}

	public static int getGameMode() {
		return gameMode;
	}
	
	public int getScreenWeigh() {
		return screenWidth;
	}
	
	public int getScreenHeigh() {
		return screenHeight;
	}
	

	public static void setSearchframe(Object searchframe) {
		MainWindowUI.searchJFrame = searchframe;
	}

	public static Netmode getNetmode() {
		return netmode;
	}

	public static void setNetmode(Netmode netmode) {
		MainWindowUI.netmode = netmode;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					mainFrame = new MainWindowUI();
					mainFrame.setVisible(true);
					mainFrame.initializeBuffer();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	/**
	 * Create the frame.
	 */
	public MainWindowUI() {
		ID = randomID();
		initializeWindow();
		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(windowStartX, windowStartY, 1024, 768);

		contentPane = new JPanel(){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				g2d.setColor(new Color(255, 255, 255, 30));
				g2d.fillRoundRect(3, 3, getWidth() - 6, getHeight() - 6, 20, 20);

				ImageIcon backGround = new ImageIcon(getClass().getResource("/resources/background/startBackgroud.png"));
				g2d.drawImage(backGround.getImage(), 0, 0, contentPane.getWidth(), contentPane.getHeight(), contentPane);

				g2d.setColor(Color.darkGray);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRoundRect(400, 200, 220, 395, 20, 20);
				
//				setShape(new Ellipse2D.Float (0, 0, getWidth (), getHeight ()));
//				g2d.drawArc(1, 1, getWidth() - 3, getHeight() - 3, 0, 360);
				
				g2d.drawRoundRect(1, 1, getWidth() - 3, getHeight() - 3, 20, 20);
			}
		};
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (getWidth() != (int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()) {
					setLocation(jframeX + (e.getXOnScreen() - mouseOnSceenX), jframeY + (e.getYOnScreen() - mouseOnSceenY));
				}
			}
		});
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mouseOnSceenX = e.getXOnScreen();
				mouseOnSceenY = e.getYOnScreen();
				jframeX = getX();
				jframeY = getY();
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setBounds(344, 176, 334, 427);
		contentPane.add(panel);
		panel.setLayout(null);

		JButton buttonAI = new MyJButton();
		buttonAI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startMusic.stopPlay();
				gameMode = 1;
				setVisible(false);
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							localPlayFrame = new LocalPlayFrame();
							localPlayFrame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		buttonAI.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonAI.setText("\u4EBA\u673A\u5BF9\u6218");
		buttonAI.setBounds(72, 46, 185, 66);
		panel.add(buttonAI);

		JButton buttonNet = new MyJButton();
		buttonNet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startMusic.stopPlay();
				gameMode = 2;
				setVisible(false);
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							ServerSelect frame = new ServerSelect();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		buttonNet.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonNet.setText("\u7F51\u7EDC\u5BF9\u6218");
		buttonNet.setBounds(72, 140, 185, 66);
		panel.add(buttonNet);

		JButton buttonLocal = new MyJButton();
		buttonLocal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startMusic.stopPlay();
				gameMode = 3;
				setVisible(false);
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							localPlayFrame = new LocalPlayFrame();
							localPlayFrame.setPlaying(true);
							localPlayFrame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		buttonLocal.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonLocal.setText("\u4EBA\u4EBA\u5BF9\u6218");
		buttonLocal.setBounds(72, 231, 185, 66);
		panel.add(buttonLocal);

		JButton buttonExit = new MyJButton();
		buttonExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		buttonExit.setFont(new Font("楷体", Font.PLAIN, 30));
		buttonExit.setText("\u9000\u51FA\u6E38\u620F");
		buttonExit.setBounds(72, 333, 185, 66);
		panel.add(buttonExit);
	}
	
	private void initializeWindow() {
		
		windowStartX = screenWidth/2 - 550;
		windowStartY = screenHeight/2 - 450;
		
		if (screenHeight < 900) {
			windowStartX = screenWidth/2 - 550;
			windowStartY = screenHeight/2 - 400;
		}
	}
	
	public String randomID() {
		String ID = "";
		try {
			ID = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 16; i++) {
			ID += (char) (Math.abs((int) (Math.random() * 26)) + 65);
		}
		ID += System.currentTimeMillis();
		return ID;
	}
	
	private void deleteFile(File file){
		File[] files = file.listFiles();
		for(int i=0; i<files.length; i++){
			files[i].delete();
		}
	}
	
	private void initializeBuffer() {
		File bufferFile = new File(bufferFolder);
		if (bufferFile.exists()) {
			deleteFile(bufferFile);
		}
	}

}
