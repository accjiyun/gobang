﻿package com.gobang.netmode.serverdata.WAN;

import java.util.Stack;
import java.util.Vector;

import com.gobang.netmode.util.ClientDataClass;
import com.gobang.netmode.util.RePlayClass;
import com.gobang.netmode.util.ServerMessage;
import com.gobang.util.chessdata.ChessPoint;

/*
 * 广域网的房间，不需要监听端口
 */
public class ServerRoom {
	//一个房间应有的信息，房主呢称,对手呢称,iP,人数,状态,端口
		private  ServerMessage serverMessage;
		
		public Vector<ServerForRoomClientOfWAN> clients = new Vector<ServerForRoomClientOfWAN>();
		 Vector<ClientDataClass> clientdataVec =new Vector<ClientDataClass>(); 
		 Stack<ChessPoint> chessall=new Stack<ChessPoint>();//存放棋子的堆栈
		 boolean isWork;
		 RePlayClass rePlayClass=new RePlayClass();
		 ClientOperationOfWAN clientOperationOfWAN = new ClientOperationOfWAN();
	    private  String IDOfChess;
	    
	    
	 // 构造方法
		public ServerRoom(ServerMessage serverMessage) {
			this.serverMessage = serverMessage;
			isWork=true;
		}
		
		
		public ClientOperationOfWAN getClientOperationOfWAN() {
			return clientOperationOfWAN;
		}
		public void setHouseOwner(ClientDataClass client){
			serverMessage.setHouseOwnerNameString(client.getClientName());
			serverMessage.setServerAddress(client.getClientAddress());
			client.setAttribute(1);
		}
		public  String getIDOfChess() {
			return IDOfChess;
		}
		public  void setIDOfChess(String iDOfChess) {
			IDOfChess = iDOfChess;
		}
		public void setOpponent(ClientDataClass client){
			serverMessage.setOppNameString(client.getClientName());
			serverMessage.setOppAddress(client.getClientAddress());
			client.setAttribute(2);
		}
		public  Stack<ChessPoint> getChessall() {
			return chessall;
		}
		public  ServerMessage getServerMessage() {
			return serverMessage;
		}
		public  Vector<ServerForRoomClientOfWAN> getClients() {
			return clients;
		}
		
		public  boolean isBegin() {
			return serverMessage.isBegin();
		}
		public  Vector<ClientDataClass> getClientdataVec() {
			return clientdataVec;
		}
		public  void setChessall(Stack<ChessPoint> chessall) {
			this.chessall = chessall;
		}
		
		public  synchronized void addClientDataClass(ClientDataClass clientDataClass){
			clientdataVec.add(clientDataClass);
		}
		public  synchronized void removeClientDataClass(int index){
			clientdataVec.remove(index);
		}
		public  Vector<ClientDataClass> getClientDatas(){
			return clientdataVec;
		}
		
		
		public  synchronized void addServerThread(ServerForRoomClientOfWAN serverThread){
			clients.add(serverThread);
		}
		public  synchronized void removeServerThread(int index){
			clients.remove(index);
		}
		public  Vector<ServerForRoomClientOfWAN> getServerThread(){
			return clients;
		}
		
		public  synchronized  void setisBegin(boolean b) {
			serverMessage.setBegin(b);
		}
		public  boolean getIsWork(){
			return isWork;
		}
		public  void setIsWork(boolean b){
			isWork=b;
		}
		
		public void remove(ServerForRoomClientOfWAN serverThread){
			clients.remove(serverThread);
			clientdataVec.remove(serverThread.client);
			if(clients.size() <= 0){//销毁房间
				this.isWork = false;
				ServerOfWAN.getServerRooms().remove(this);
				ServerOfWAN.getServerHashtable().remove(serverMessage.getID());
				ServerOfWAN.getServerMessageStack().remove(serverMessage);
				//释放空间
				clients.clear();
				clientdataVec.clear();
				chessall.clear();
				clients = null;
				clientdataVec = null;
				chessall = null;
				rePlayClass = null;
				IDOfChess = null;
			}else{
				serverMessage.setRoomNum(clients.size());
			}
		}
		
		@Override
		public boolean equals(Object obj) {
		// TODO Auto-generated method stub
			if(obj != null){
				if( this.getServerMessage() != null && ((ServerRoom)obj).getServerMessage() != null ){
				return	this.getServerMessage().getID().equals(((ServerRoom)obj).getServerMessage().getID());
				} 
			}
			return false;
		}
}
