﻿package com.gobang.netmode.serverdata.WAN;

import java.net.Socket;

import com.gobang.netmode.sendandreceive.Message;
import com.gobang.netmode.sendandreceive.MessageMSG;
import com.gobang.netmode.sendandreceive.Order;
import com.gobang.netmode.sendandreceive.SendAndReceive;

public class ServerForVisitor extends Thread {
	SendAndReceive sendAndReceive;
	Message message = new Message();
	boolean isWork = false;

	public ServerForVisitor(Socket socket) {
		// TODO Auto-generated constructor stub
		sendAndReceive = new SendAndReceive(socket);
		isWork = true;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
			while(isWork){
				try{
					message = sendAndReceive.receiveMessage();
					if(message != null){
						System.out.println("ServerForVisitor.run()"+ message);
						operation(message);
					} else{
						isWork = false;
						message = null;
					}
				}catch (ClassNotFoundException e) {
					e.printStackTrace();
					isWork = false;
					message = null;
				}
			}
			message = null;
	}
	private void operation(Message message) {
		Message m = new Message();
		ServerRoom serverRoom;
		switch (message.order) {
		case ASK_ROOMMSG:
			if(ServerOfWAN.getServerMessageStack().size() >0){
				m.msg = MessageMSG.SERVER_MESSAGE_VECTOR + MessageMSG.ORDER;
				m.order = Order.ALL_ROOMMSG;
				m.serverMessageVector = ServerOfWAN.getServerMessageStack();
			} else{
				m.msg = MessageMSG.ORDER + MessageMSG.SERVER_MESSAGE_VECTOR;
				m.order = Order.NULL;
				m.serverMessageVector = null;
			}
			sendAndReceive.sendMessage(m);
			break;
		case CREATE_ROOM:
			 serverRoom = ServerOfWAN.CreateRoom(message.clientDataClass);
			if(serverRoom != null){//房间创建成功
				message.msg = MessageMSG.ORDER;
				message.order =Order.CREATE_ROOM_SUCCESS;
				isWork = false;
			} else{
				message.msg = MessageMSG.ORDER;
				message.order =Order.CREATE_ROOM_FAIL;
			}
			sendAndReceive.sendMessage(message);
			if(!isWork){
				try {
					ServerForRoomClientOfWAN client =new ServerForRoomClientOfWAN(sendAndReceive, serverRoom);//把房间的服务器给所有的客户线程，方便访问数据
					serverRoom.getClients().add(client);
					client.start();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			break;
		case JOIN :
			System.out.println("ServerForVisitor.operation()"+message.clientDataClass+"加入房间："+message.serverMessage);
			if(message.clientDataClass != null && message.serverMessage != null){
				 serverRoom = (ServerRoom)ServerOfWAN.getServerHashtable().get(message.serverMessage.getID());
				 System.out.println("ServerForVisitor.operation()对应的房间："+serverRoom);
				if(serverRoom != null){
					isWork = false;
					try {
						ServerForRoomClientOfWAN client =new ServerForRoomClientOfWAN(sendAndReceive, serverRoom);//把房间的服务器给所有的客户线程，方便访问数据
						serverRoom.getClients().add(client);
						client.start();
						message.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.ORDER;
						sendAndReceive.sendMessage(message);
//						client.serverRoom.getClientOperationOfWAN().operation(client, message);//发送加入的信息
					} catch (Exception e) {
						// TODO: handle exception
					}	
				}else{//加入房间失败
					System.out.println("ServerForVisitor.operation()加入房间失败"+message.clientDataClass);
					m.msg = MessageMSG.ORDER;
					m.order = Order.JOIN_FAIL;
					sendAndReceive.sendMessage(m);
				}
			}
			break;
		default:
			break;
		}
	}
	
}
