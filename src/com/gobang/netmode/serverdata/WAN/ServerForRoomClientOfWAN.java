﻿package com.gobang.netmode.serverdata.WAN;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Stack;
import com.gobang.netmode.sendandreceive.Message;
import com.gobang.netmode.sendandreceive.Order;
import com.gobang.netmode.sendandreceive.SendAndReceive;
import com.gobang.netmode.serverdata.LAN.MessageLog;
import com.gobang.netmode.util.ClientDataClass;



// 声明服务端线程：循环接收客户端信息，完成信息的翻译，并根据信息反馈给所有的客户
public class ServerForRoomClientOfWAN extends Thread {
	ServerRoom serverRoom = null;
	public SendAndReceive sendAndReceive ;
	ClientDataClass client = null;
	Message message = new Message();
	boolean isRun=true;
	Stack<MessageLog> receiveMessageStack=new Stack<MessageLog>();
	Stack<MessageLog> sendMessageStack=new Stack<MessageLog>();
	
	public ServerForRoomClientOfWAN(SendAndReceive sendAndReceive, ServerRoom serverRoom) throws FileNotFoundException { // 构造方法
		// 每构造一个线程，说明就有一个客户端连上，立即向客户端发送链接成功及房间信息
		this.sendAndReceive = sendAndReceive;
		this.serverRoom = serverRoom;
	    File file=new File("debug");  
	  //如果文件夹不存在则创建  
	  if  (!file .exists()  && !file .isDirectory()){     
	      file .mkdirs();  
	  } 
		String filenameOut= "debug\\ServerOut"+this+".txt";
		String filenameErr ="debug\\ServerErr"+this+".txt";
		PrintStream outPrintStream=new PrintStream(filenameOut);
		PrintStream errStream =new PrintStream(filenameErr);
//		System.setOut(outPrintStream);
//		System.setErr(errStream);
	}

	public void run() {
		try {
			while (serverRoom.getIsWork()&& isRun) {
					message = sendAndReceive.receiveMessage();
					receiveMessageStack.push(new MessageLog(message));
					System.out.println("ServerForRoomClientOfWAN.run()收到客户端消息："+message);
					if (message != null) {//能收到消息
						if(client == null){
							client = message.clientDataClass;
							System.out.println("ServerForRoomClientOfWAN.run()"+serverRoom.clientdataVec.size());
						}
						if (message.order != Order.NULL) {// 从解析出来的信息中含有命令操作，则执行相应的操作
							serverRoom.getClientOperationOfWAN().operation(this, message);// 对命令进行操作，还要知道对象是this这个线程
						}
					} else {
						System.out.println("ServerForRoomClientOfWAN:该用户下线或无法收到消息" + client);
						isRun=false;
						sendAndReceive.close();
						if(client != null){
						serverRoom.remove(this);// 删除该线程的信息
						}
					}
			}//while (server.getIsWork()&& isRun) end
		} catch (ClassNotFoundException e) {
			System.out.println(this + "ServerForRoomClientOfWAN: 无法收到客户端信息,ClassNotFoundException" + client);
			e.printStackTrace();
		}
	}// run() end
@Override
public boolean equals(Object obj) {
	if(obj == null || this.client == null){
		return false;
	}
	if(((ServerForRoomClientOfWAN)obj).client != null){
		if(((ServerForRoomClientOfWAN)obj).client.equals(this.client)){
			return true;
		}else{
			return false;
		}
	}else{
		return  false;
	}
}
	public  synchronized boolean sendAClient(Message message) {
		// 向自己发送数据流
		System.out.println("ServerForRoomClient.sendAClient()向该用户发送单独的消息："+message);
			if (sendAndReceive.sendMessage(message))// 打包好发给用户的数据
			{
				sendMessageStack.push(new MessageLog(message));
				return true;
			} else {
				return false;
			}
	}
	public  synchronized boolean sendAClient(ServerForRoomClientOfWAN s, Message message) {
		// 向该用户发送数据流
			if (s.sendAndReceive.sendMessage(message))// 打包好发给用户的数据
			{
				s.sendMessageStack.push(new MessageLog(message));
				return true;
			} else {
				return false;
			}
	}

	public synchronized int sendClients(Message message) {// 向每一位客户发送数据流
		// 向该用户发送数据流,返回发送成功的用户人数
		int count = 0;
		int index;
		ServerForRoomClientOfWAN s;
		for (index = 0; index < serverRoom.clients.size(); index++) {
			s = serverRoom.clients.get(index);
			if (s.sendAndReceive.sendMessage(message)) // 打包好发给用户的数据
			{
				s.sendMessageStack.push(new MessageLog(message));
				count++;
			} else {
				serverRoom.clients.remove(index);
			}
		}
		return count;
	}

	public synchronized int sendClientExceptOwn(Message message) {
		// 向该用户发送数据流,返回发送成功的用户人数
		int count = 0;
		ServerForRoomClientOfWAN s;
		for (int index = 0; index < serverRoom.getServerThread().size(); index++) {
			s = serverRoom.getServerThread().get(index);
			if (!s.equals(this)) {
				if (s.sendAndReceive.sendMessage(message)) // 打包好发给用户的数据
				{
					s.sendMessageStack.push(new MessageLog(message));
					count++;
				} else {
					serverRoom.remove(s);// 尽快删除该线程
					System.out.println("ServerThread.sendClientExceptOwn()失败，该线程已被删除！" + s.client);
				}
			}
		}
		return count;
	}
}
