﻿package com.gobang.netmode.serverdata.WAN;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Stack;
import java.util.Vector;

import com.gobang.netmode.util.ClientDataClass;
import com.gobang.netmode.util.ServerMessage;


public class ServerOfWAN extends Thread {
	private static Stack<ServerMessage> serverMessageStack = new Stack<>();
	
//	Vector<ServerForVisitor> serverForVisitors = new Vector<>();//保存游客线程信息
	private static Vector<ServerRoom> serverRooms= new  Vector<ServerRoom>();//保存各个房间的服务器
	private static ServerHashtable serverHashtable = new ServerHashtable(500);//建立500个房间
	
	final int portOfWAN = 36871;
	private static boolean isWork = false;
	private static boolean isRun = false;// 只要还有房间开着，isRun= true， 会尝试重新建立连接；
	
	
	private static ServerSocket serverSocketOfWAN = null;

	public ServerOfWAN() {
		// TODO Auto-generated constructor stub
		isWork = true;
		isRun = true;
	}
	
	public static Vector<ServerRoom> getServerRooms() {
		return serverRooms;
	}

	public static ServerHashtable getServerHashtable() {
		return serverHashtable;
	}


	public static boolean isWork() {
		return isWork;
	}


	public static void setWork(boolean isWork) {
		ServerOfWAN.isWork = isWork;
	}


	public static boolean isRun() {
		return isRun;
	}


	public static void setRun(boolean isRun) {
		ServerOfWAN.isRun = isRun;
	}


	public int getPortOfWAN() {
		return portOfWAN;
	}


	public static Stack<ServerMessage> getServerMessageStack() {
		return serverMessageStack;
	}


	public void run() {
		while (isRun) {
			while (isWork) {
				if (serverSocketOfWAN == null) {
					try {
						serverSocketOfWAN = new ServerSocket(portOfWAN, 500);
						System.out.println("ServerOfWAN.run()监听成功！"+serverSocketOfWAN);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} // if (serverSocketOfWAN == null) end
				try {
					Socket socket = serverSocketOfWAN.accept();
					System.out.println("ServerOfWAN.run()链接成功"+socket);
					ServerForVisitor serverForVisitor = new ServerForVisitor(socket);
					serverForVisitor.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(5000);
				serverSocketOfWAN.close();
				serverSocketOfWAN = null;
				isWork = true;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static synchronized ServerRoom CreateRoom(ClientDataClass houseOwnClient) {
		ServerMessage serverMessage = new ServerMessage(houseOwnClient);
		ServerRoom serverRoom = new ServerRoom (serverMessage);//**********把服务器的房间存如哈希表中
		if(serverHashtable.put(serverMessage.getID(), serverRoom) != null){//创建成功
			serverRooms.add(serverRoom);
			serverMessageStack.add(serverMessage);
			System.out.println("ServerOfWAN.CreateRoom()房间数：serverMessageStack.size()："+serverMessageStack.size()+" serverHashtable.size() :"+serverHashtable.size());
			return serverRoom;
		}
		//创建失败，房间太多
		return null;
	}
	public static void main(String[] args) {
		ServerOfWAN serverOfWAN = new ServerOfWAN();
		serverOfWAN.start();
		System.out.println("ServerOfWAN.main()总服务器运行中……"+serverOfWAN);
	}
}
