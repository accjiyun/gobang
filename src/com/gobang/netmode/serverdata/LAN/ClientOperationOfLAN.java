﻿package com.gobang.netmode.serverdata.LAN;

import java.io.IOException;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import com.gobang.netmode.sendandreceive.Message;
import com.gobang.netmode.sendandreceive.MessageMSG;
import com.gobang.netmode.sendandreceive.Order;
import com.gobang.netmode.util.ClientDataClass;

public class ClientOperationOfLAN {
	
	public synchronized void operation(ServerForRoomClientOfLAN serverForRoomClient,//把多线程的操作去掉，方便不同的服务器可以访问无阻S
			Message message) {// 对某个信息进行操作
		switch (message.order) {
		case ASK_ROOMMSG://对于局域网中的游客而言
			message = serverForRoomClient.getRoomMessage();
			serverForRoomClient.sendAClient(message);
			break;
		case UPDATA://更新本地用户的所以信息
			 message= new Message();
			 message.isBegin= serverForRoomClient.server.isBegin();
			 message.msg = MessageMSG.IS_BEGIN;
			 if(serverForRoomClient.server.getClientdataVec().size() > 0){
				 message.clientDatas=serverForRoomClient.server.getClientdataVec();
				 message.msg += MessageMSG.CLIENTD_DATAS;
			 }
			 if(serverForRoomClient.server.getChessall().size() > 0 ){
				 message.chessall=serverForRoomClient.server.getChessall();
				 message.msg +=MessageMSG.CHESS_ALL;
			 }
			 serverForRoomClient.sendAClient(message);
			 System.out.println("ServerOperation.operation()UPDATA:发送成功");
			break;
		case AGREE_REPLAY:
			if(!serverForRoomClient.server.getIDOfChess().equals(message.IDOfChess)){
				break;
			}
			if(message.clientDataClass.getAttribute()==1 ){
				serverForRoomClient.server.rePlayClass.setHouseOwner(1);
				}else{
					serverForRoomClient.server.rePlayClass.setOpponent(1);
				}
			System.out.println("ServerOperation.operation()有一方同意重新开始："+serverForRoomClient.server.rePlayClass);
			if(message.clientDataClass.getAttribute() == 1){//房主同意开始
				if(serverForRoomClient.server.rePlayClass.getOpponent() == 0){
					serverForRoomClient.server.rePlayClass.init();//重置
					message.order=Order.DISAGREE_REPLAY;
					serverForRoomClient.sendAClient(message);//向自己发送对方不同意开始比赛
				}else if(serverForRoomClient.server.rePlayClass.getOpponent() == 1){//可以重新开始比赛
					serverForRoomClient.server.rePlayClass.init();//重置
					serverForRoomClient.server.setisBegin(true); 
					serverForRoomClient.server.chessall.clear();
					serverForRoomClient.server.setIsWork(true);
					serverForRoomClient.server.setIDOfChess(""+System.currentTimeMillis());
					message=new Message();
					message.clientDataClass=serverForRoomClient.server.clientdataVec.get(0);
					message.oppClientDataClass=serverForRoomClient.server.clientdataVec.get(1);
					message.order=Order.AGREE_REPLAY;
					message.IDOfChess = serverForRoomClient.server.getIDOfChess();
					message.msg =MessageMSG.CLIENT_DATA_CLASS + MessageMSG.OPP_CLIENT_DATA_CLASS +MessageMSG.ORDER+MessageMSG.ID_OF_CHESS;
					serverForRoomClient.sendClients(message);//发送比赛开始信息
				}
			}else{//对手同意开始
				if(serverForRoomClient.server.rePlayClass.getHouseOwner() == 0){
					serverForRoomClient.server.rePlayClass.init();//重置
					message.order = Order.DISAGREE_REPLAY;
					message.msg = MessageMSG.ORDER;
					serverForRoomClient.sendAClient(message);//向自己发送对方不同意开始比赛
				}else if(serverForRoomClient.server.rePlayClass.getHouseOwner() == 1){//可以重新开始比赛
					serverForRoomClient.server.rePlayClass.init();//重置
					serverForRoomClient.server.setisBegin(true); 
					serverForRoomClient.server.chessall.clear();
					serverForRoomClient.server.setIsWork(true);
					serverForRoomClient.server.setIDOfChess(""+System.currentTimeMillis());
					message=new Message();
					message.clientDataClass=serverForRoomClient.server.clientdataVec.get(0);
					message.oppClientDataClass=serverForRoomClient.server.clientdataVec.get(1);
					message.order=Order.AGREE_REPLAY;
					message.IDOfChess = serverForRoomClient.server.getIDOfChess();
					message.msg =MessageMSG.CLIENT_DATA_CLASS + MessageMSG.OPP_CLIENT_DATA_CLASS +MessageMSG.ORDER+MessageMSG.ID_OF_CHESS;
					serverForRoomClient.sendClients(message);//发送比赛开始信息
				}
			}
			break;
		case DISAGREE_REPLAY://双方都不同意开始则什么都不做
			if(!serverForRoomClient.server.getIDOfChess().equals(message.IDOfChess)){
				break;
			}
			if(message.clientDataClass.getAttribute()==1 ){
			serverForRoomClient.server.rePlayClass.setHouseOwner(0);
		     System.out.println("ServerOperation.operation()房主不同意重新开始："+serverForRoomClient.server.rePlayClass);
				if(serverForRoomClient.server.rePlayClass.getOpponent() == 1){//对手已经答应开始了
					serverForRoomClient.server.rePlayClass.init();//重置
					message.order = Order.DISAGREE_REPLAY;
					message.msg = MessageMSG.ORDER;
					serverForRoomClient.server.getClients().get(1).sendAClient(message);//向对方发送不同意开始比赛
				}
			}
			else{//对手选择了放弃重新开始
				serverForRoomClient.server.rePlayClass.setOpponent(0);
				System.out.println("ServerOperation.operation()对手放弃了重新开始："+serverForRoomClient.server.rePlayClass);
				if(serverForRoomClient.server.rePlayClass.getHouseOwner() == 1){//对手已经答应开始了
					serverForRoomClient.server.rePlayClass.init();//重置
					message.order=Order.DISAGREE_REPLAY;
					message.msg = MessageMSG.ORDER;
					serverForRoomClient.server.getClients().get(0).sendAClient(message);//向房主发送对方不同意开始比赛
				}
			}
			break;
		case SEARCH://局域网特有
		Message m2 =new Message();
			m2 = serverForRoomClient.getRoomMessage();
			System.out.println("ClientOperationOfLAN.operation()sendAClient发送成功："+serverForRoomClient.sendAClient( m2));
			serverForRoomClient.server.remove(serverForRoomClient);
			Timer timer = new Timer();
			TimerTask closeLater = new TimerTask(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					serverForRoomClient.sendAndReceive.close();
					
				}
			};
			timer.schedule(closeLater, 3000);
			break;
		case WIN:
			serverForRoomClient.server.setisBegin(false);
			serverForRoomClient.sendClientExceptOwn(message);
			break;
		case RECONECT://先针对局域来操作
				if(message.clientDataClass.getAttribute() == 1){//找到房主
					serverForRoomClient.server.setHouseOwner(message.clientDataClass);
					if(serverForRoomClient.server.clientdataVec.size()>0){
						if(serverForRoomClient.server.clientdataVec.get(0).getAttribute() ==1){
							serverForRoomClient.server.clientdataVec.get(0).setAttribute(4);
						}
					}
					serverForRoomClient.server.clientdataVec.add(0, message.clientDataClass);
				}else{
					System.out.println("ServerOperation.operation()服务器中客户人数："+serverForRoomClient.server.clientdataVec.size());
					if(serverForRoomClient.server.clientdataVec.size() <= 1){
						serverForRoomClient.server.setOpponent(message.clientDataClass);
					}
					serverForRoomClient.server.clientdataVec.add(message.clientDataClass);
				}
				Collections.sort(serverForRoomClient.server.clientdataVec);//从新排序
				message.clientDatas = serverForRoomClient.server.getClientDatas();
				message.msg += MessageMSG.CLIENTD_DATAS;
				serverForRoomClient.sendClients(message);
			break;
		case JOIN:// 判断属性，把客户信息加入客户堆栈，发送命令给客户更新本地信息
			if (serverForRoomClient.server.getClientdataVec().size() == 0) {// 成为房主
				serverForRoomClient.server.setHouseOwner(message.clientDataClass);
				System.out.println("ServerOperation.operation()更新后的数据："+message.clientDataClass);
				serverForRoomClient.server.clientdataVec.add(message.clientDataClass);
				
//			    message=new Message();
//			    message.clientDataClass=serverForRoomClient.server.clientdataVec.get(0);
//			    message.order = Order.UPDATA_YOUSELF;
//			    message.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.ORDER;
//				if (!serverForRoomClient.sendAClient(message)) {
//					System.out.println("服务器发给房主的信息失败！"+message);
//				}else{
//					System.out.println("ServerOperation.operation()服务器发送成功！\n"+message);
//				}

			} else if (serverForRoomClient.server.getClientdataVec().size() == 1) {// 成为对手
				
				serverForRoomClient.server.setOpponent(message.clientDataClass);
				
				serverForRoomClient.server.getClientdataVec().add(message.clientDataClass);
//				Message m = new Message();
//				 m.clientDataClass=message.clientDataClass;
//				 m.order = Order.UPDATA_YOUSELF;
//				 m.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.ORDER;
//				if (!serverForRoomClient.sendAClient(m)) {
//					System.out.println("服务器发给对手的信息失败！"+m);
//				}else{
//					System.out.println("ServerOperation.operation()服务器发送成功！\n"+m);
//				}
			} else if (serverForRoomClient.server.getClientdataVec().size() > 1) {// 成为观众
				message.clientDataClass.setAttribute(4);
				serverForRoomClient.server.getClientdataVec().add(message.clientDataClass);
//				Message m=new Message();
//				 m.clientDataClass=message.clientDataClass;
//				 m.order = Order.UPDATA_YOUSELF;
//				 m.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.ORDER;
//				if (!serverForRoomClient.sendAClient(m)) {
//					System.out.println("服务器发给观众的信息失败！"+m);
//				}else{
//					System.out.println("ServerOperation.operation()服务器发送成功！\n"+m);
//				}
			}
			serverForRoomClient.sendClients(message);
//			int i;
//			if ((i = serverForRoomClient.sendClientExceptOwn(message)) != serverForRoomClient.server.clientdataVec.size() - 1) {// 发送加入提示
//				System.out.println("服务器发给所有客户的信息失败！ 只发送了" + i + "份， 共有  "
//						+ (serverForRoomClient.server.clientdataVec.size() - 1) + " 个用户");
//				System.out.println("ServerOperation.operation()"+message);
//			}
			break;
		case EXIT:
				if(message.clientDataClass.equals(serverForRoomClient.server.clientdataVec.get(0))){//房主退出
					serverForRoomClient.server.remove(serverForRoomClient);
					serverForRoomClient.isRun = false;
					serverForRoomClient.sendAndReceive.close();
					serverForRoomClient.sendClientExceptOwn(message);//告诉所有人房主退出
					
					if (serverForRoomClient.server.clientdataVec.size() > 0) {
						Message m = new Message();
						m.clientDataClass = serverForRoomClient.server.clientdataVec.get(0);
						if (serverForRoomClient.server.isBegin()) {
							m.order = Order.WIN;
							m.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.ORDER;
							serverForRoomClient.sendClientExceptOwn(m);// 赢棋
						}
						m.order = Order.SET_SERVER;
						serverForRoomClient.sendClientExceptOwn(m);// 重新在另一处开启服务器
					}
					//关闭服务器
					serverForRoomClient.server.isWork=false;
					Timer timer1 = new Timer();//一秒后关闭服务器
				    CloseLater closeLater1 = new CloseLater(serverForRoomClient.server);
					timer1.schedule(closeLater1, 1000);	
					
				}else if(serverForRoomClient.server.clientdataVec.size() > 1 
						&& message.clientDataClass.equals(serverForRoomClient.server.clientdataVec.get(1))){//对手退出
					
					serverForRoomClient.server.remove(serverForRoomClient);
					serverForRoomClient.isRun = false;
					serverForRoomClient.sendAndReceive.close();
					serverForRoomClient.sendClientExceptOwn(message);//告诉所有人对手退出
					
					if(serverForRoomClient.server.isBegin()){
						serverForRoomClient.server.setisBegin(false);
						Message m = new Message();
						m.clientDataClass = serverForRoomClient.server.clientdataVec.get(0);
						m.order = Order.WIN;
						m.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.ORDER;
						serverForRoomClient.sendClientExceptOwn(message);// 赢棋
					}
				}else{//其他人退出
					serverForRoomClient.server.remove(serverForRoomClient);
					serverForRoomClient.isRun = false;
					serverForRoomClient.sendAndReceive.close();
					serverForRoomClient.sendClientExceptOwn(message);
				}
			break;
		case LAST_CHESS:// 服务器肯定能收到每颗棋子的信息，如果服务器收不到，那其他客户端肯定也收不到
			serverForRoomClient.server.chessall.push(message.chesspoint);
			serverForRoomClient.sendClientExceptOwn(message);
			break;
		case CHAT:
		case FILE:
			serverForRoomClient.sendClientExceptOwn(message);
			break;
		case SECRET_CHAT:// message.oppClientDataClass判断发送对象
			for (ServerForRoomClientOfLAN s : serverForRoomClient.server.clients) {
				if (s.client.equals(message.oppClientDataClass)){
					s.sendAClient(message);
				}
			}
			break;
		case APPLY_FIGHT:// 改变用户属性，调整用户列表顺序 
			for(ClientDataClass c : serverForRoomClient.server.clientdataVec){
				if(c.equals(message.clientDataClass)){
				    if(c.getAttribute() != 3){
				    	c.setAttribute(3);
				    }else{
				    	c.setAttribute(4);
				    }
				}
			}
			Collections.sort(serverForRoomClient.server.clientdataVec);
			message = new Message();// 更新所有客户端客户列表
			message.clientDatas = serverForRoomClient.server.clientdataVec;
			message.msg = MessageMSG.CLIENTD_DATAS;
			serverForRoomClient.sendClients(message);
			break;
		case ASK_PEACE:
		case BACK:// 悔棋
			if (message.clientDataClass.getAttribute() == 1)// 房主发出信息	
			serverForRoomClient.server.clients.get(1).sendAClient(message);// 向对手发出求和信息
			else {
				serverForRoomClient.server.clients.get(0).sendAClient(message);// 向房主发出信息
			}
			break;
		case AGREE_BACK://同步服务器和客户端的棋子
			serverForRoomClient.server.setisBegin(true);
			serverForRoomClient.server.chessall.clear();
			serverForRoomClient.server.chessall = message.chessall;
			serverForRoomClient.sendClientExceptOwn(message);
			break;
		case GIVEUP:
		case AGREE_ASKPEACE:
			serverForRoomClient.server.setisBegin(false);
		case DISAGREE_ASKPEACE:
		case DISAGREE_BACK:
			serverForRoomClient.sendClientExceptOwn(message);// 向除他自己以外的用户发送信息
			break;
		case KICK: // 被踢的人接到消息后，自己向服务器发送离开服务器的请求
			for (ServerForRoomClientOfLAN s : serverForRoomClient.server.clients) {
				if (s.client.equals(message.oppClientDataClass)){
					if(serverForRoomClient.server.isBegin() && message.oppClientDataClass.equals(serverForRoomClient.server.clientdataVec.get(1))){
					 Message m = new Message();
					 m.order = Order.KICK_FAIL;
					 serverForRoomClient.sendAClient(m);
					}else{
						s.sendAClient(message);
					}
				}
			}
			break;
		case KICKED:
			serverForRoomClient.server.remove(serverForRoomClient);
			serverForRoomClient.isRun = false;
			serverForRoomClient.sendAndReceive.close();
			message.clientDatas = serverForRoomClient.server.clientdataVec;
			message.msg += MessageMSG.CLIENTD_DATAS;
			serverForRoomClient.sendClientExceptOwn(message);
			break;
		case BEGIN:
			if((message.msg & MessageMSG.OPP_CLIENT_DATA_CLASS) == MessageMSG.OPP_CLIENT_DATA_CLASS){//服务器收到开始比赛的命令，判断是否选中了对象
				for (ServerForRoomClientOfLAN s : serverForRoomClient.server.clients) {
					if (s.client.equals(message.oppClientDataClass)){
						s.sendAClient(message);
					}
				}
			}
			else{
			serverForRoomClient.server.clients.get(1).sendAClient(message);// 向对手发出开始信号
			}
			break;
		case AGREE_BEGIN:// 游戏被初始化,客户信息列表也被更新
			ClientDataClass c=null;
			for (int index=0; index < serverForRoomClient.server.clientdataVec.size(); index++) {
				c = serverForRoomClient.server.clientdataVec.get(index);
				if (c.equals(message.clientDataClass)){
					c.setAttribute(2);
					serverForRoomClient.server.setOpponent(c);
					Collections.sort(serverForRoomClient.server.clientdataVec);
				}else if(c.getAttribute() == 2){
					c.setAttribute(3);
				}
			}
			serverForRoomClient.server.setIDOfChess(message.IDOfChess);
			serverForRoomClient.server.setisBegin(true); 
			serverForRoomClient.server.chessall.clear();
			serverForRoomClient.server.setIsWork(true);
			message = new Message();
			message.clientDataClass = serverForRoomClient.server.clientdataVec.get(0);
			message.oppClientDataClass = serverForRoomClient.server.clientdataVec.get(1);
			message.order = Order.AGREE_BEGIN;
			message.IDOfChess = serverForRoomClient.server.getIDOfChess();
			message.clientDatas = serverForRoomClient.server.getClientdataVec();
			message.msg = MessageMSG.CLIENT_DATA_CLASS + MessageMSG.OPP_CLIENT_DATA_CLASS + MessageMSG.ORDER + MessageMSG.CLIENTD_DATAS+MessageMSG.ID_OF_CHESS;
			serverForRoomClient.sendClients(message);
			break;
		case DISAGREE_BEGIN:
			serverForRoomClient.server.clients.get(0).sendAClient(message);// 向房主反馈不同意开始游戏
			break;
		case SET_BEGIN:// 设置游戏双方//客户端没做该指令*********************************************************
			break;
		case ASK_ALL_CHESS:// 获取所有棋子请求
			Message m = new Message();
			m.msg = MessageMSG.CHESS_ALL;
			m.chessall = serverForRoomClient.server.getChessall();
			serverForRoomClient.sendAClient(m);// 向请求的用户发送棋子信息
			break;
		case ASK_ALL_CLIENTS:
			Message m1 = new Message();
			m1.clientDatas = serverForRoomClient.server.getClientDatas();
			m1.msg = MessageMSG.CLIENTD_DATAS;
			serverForRoomClient.sendAClient(m1);// 向请求的用户发送所有客户信息
			break;
		default:
			break;
		}

	}
	
	/*
	 * 重新设置对手，并更新所有的用户
	 */
	
}
class CloseLater extends TimerTask {
	ServerOfLAN s;
	public CloseLater(ServerOfLAN s) {
		this.s = s;
	}
	@Override
	public void run() {
		try {
			s.serverSocket.close();
			System.out.println("服务器关闭成功！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
		} catch (IOException e) {
			System.out.println("服务器关闭失败！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
			e.printStackTrace();
		}//释放服务器监听的端口，并断开与客户端的连接
	}

}
