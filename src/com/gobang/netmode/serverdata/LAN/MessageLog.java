﻿package com.gobang.netmode.serverdata.LAN;

import java.util.Date;

import com.gobang.netmode.sendandreceive.Message;


public class MessageLog {
	Message message;
	String now;
	public MessageLog(Message m) {
		// TODO Auto-generated constructor stub
		this.message = m;
		java.text.DateFormat format1 = new java.text.SimpleDateFormat("HH:mm:ss");
		this.now = format1.format(new Date());
	}
	public Message getMessage() {
		return message;
	}
	public String getNow() {
		return now;
	}
	@Override
	public String toString() {
		return ("\t"+ now +"\n" + this.message.toString());
	}
	
	
}
