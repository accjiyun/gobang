﻿package com.gobang.netmode.serverdata.LAN;

/*
 * 一个服务器相当于一个房间，使用多线程技术，创建房间的用户是房主，
 * 记录下房间里的用户ip,房主有权利设定比赛对象（他和某一个用户），
 * 当房主退出时，房间里的某一个用户成为房主，（服务器转接），
 * 直到房间里没有用户时，服务器关闭。
 * 
 */


import java.io.*;
import java.net.*;
import java.util.*;

import com.gobang.netmode.util.ClientDataClass;
import com.gobang.netmode.util.RePlayClass;
import com.gobang.netmode.util.ServerMessage;
import com.gobang.util.chessdata.ChessPoint;



public class ServerOfLAN extends Thread{
	//一个房间应有的信息，房主呢称,对手呢称,iP,人数,状态,端口
	private  ServerMessage serverMessage = new  ServerMessage();
	
	public Vector<ServerForRoomClientOfLAN> clients = new Vector<ServerForRoomClientOfLAN>();
	 Vector<ClientDataClass> clientdataVec =new Vector<ClientDataClass>(); 
	 Stack<ChessPoint> chessall=new Stack<ChessPoint>();//存放棋子的堆栈
//	 InetAddress serverIP;//服务器ip
	 boolean isWork;
	 RePlayClass rePlayClass=new RePlayClass();
	// 用ArrayList 存储链接的客户变量
	 ServerSocket serverSocket = null;//建立服务器Socket
    public  ClientOperationOfLAN clientOperationOfLAN = new ClientOperationOfLAN();//服务器的操作方式对象
    private  String IDOfChess;
    
    
    
	// 构造方法
	public ServerOfLAN() {
		isWork=true;
//		ServerJFrame serverJFrame = new ServerJFrame(this);
//		serverJFrame.setVisible(true);
	}
	public ServerOfLAN(ServerSocket serversocket) {
		serverSocket=serversocket;
		isWork=true;
	}
	public void run(){
		try {//创建一个ServerSocket
			if (serverSocket == null){
			serverSocket  =new ServerSocket(serverMessage.getPort(), 50);				
			}
			while(isWork){
				try {
					Socket socket = serverSocket.accept();
					System.out.println(socket.getInetAddress()+" 客户连接成功");
					ServerForRoomClientOfLAN client =new ServerForRoomClientOfLAN(socket, this);//把房间的服务器给所有的客户线程，方便访问数据
					clients.add(client);
					client.start();
				} catch (IOException e) {
					isWork=false;
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			System.out.println("无法在端口"+serverMessage.getPort()+ "监听");
		}
		// 关闭之前要做些事
	}
	
	/***************
	 * 定义一些函数对服务器中的数据进行操作
	 * 
	 **************/
	
	public void setHouseOwner(ClientDataClass client){
		serverMessage.setHouseOwnerNameString(client.getClientName());
		serverMessage.setServerAddress(client.getClientAddress());
		client.setAttribute(1);
	}
	public  String getIDOfChess() {
		return IDOfChess;
	}
	public  void setIDOfChess(String iDOfChess) {
		IDOfChess = iDOfChess;
	}
	public void setOpponent(ClientDataClass client){
		serverMessage.setOppNameString(client.getClientName());
		serverMessage.setOppAddress(client.getClientAddress());
		client.setAttribute(2);
	}
	public  Stack<ChessPoint> getChessall() {
		return chessall;
	}
	public  ServerMessage getServerMessage() {
		return serverMessage;
	}
	public  Vector<ServerForRoomClientOfLAN> getClients() {
		return clients;
	}
	
	public  boolean isBegin() {
		return serverMessage.isBegin();
	}
	public  Vector<ClientDataClass> getClientdataVec() {
		return clientdataVec;
	}
	public  void setChessall(Stack<ChessPoint> chessall) {
		this.chessall = chessall;
	}
	
	public  synchronized void addClientDataClass(ClientDataClass clientDataClass){
		clientdataVec.add(clientDataClass);
	}
	public  synchronized void removeClientDataClass(int index){
		clientdataVec.remove(index);
	}
	public  Vector<ClientDataClass> getClientDatas(){
		return clientdataVec;
	}
	
	
	public  synchronized void addServerThread(ServerForRoomClientOfLAN serverThread){
		clients.add(serverThread);
	}
	public  synchronized void removeServerThread(int index){
		clients.remove(index);
	}
	public  Vector<ServerForRoomClientOfLAN> getServerThread(){
		return clients;
	}
	
	public  synchronized  void setisBegin(boolean b) {
		serverMessage.setBegin(b);
	}
	public  boolean getIsWork(){
		return isWork;
	}
	public  void setIsWork(boolean b){
		isWork=b;
	}
	public void remove(ServerForRoomClientOfLAN serverThread){
		clients.remove(serverThread);
		clientdataVec.remove(serverThread.client);
		if(clients.size() <= 0){
			this.isWork = false;
			try {
				this.serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			serverMessage.setRoomNum(clients.size());
		}
	}
	public ClientOperationOfLAN getClientOperationOfLAN() {
		return clientOperationOfLAN;
	}
	
	/***************
	 * 定义一些函数对服务器中的数据进行操作结束
	 * 
	 **************/
	
	
	
}