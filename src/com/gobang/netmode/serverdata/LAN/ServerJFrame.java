﻿package com.gobang.netmode.serverdata.LAN;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;



import java.util.Stack;
import java.util.Vector;

import javax.swing.JLabel;

import java.awt.FlowLayout;

import javax.swing.JTextField;

import java.awt.Component;

import javax.swing.Box;

import java.awt.GridLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;

import com.gobang.netmode.util.ClientDataClass;

import javax.swing.event.ListSelectionEvent;

import java.awt.Font;


public class ServerJFrame extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ServerOfLAN server;
	private JPanel contentPane;
	private JTextField serverIPTextField;
	private JTextField serverPortTextField;
	private JTextField isWorkTextField;
	private JTextField clientNumTextField;
	private JTextField houseOwnerTextField;
	private JTextField OppTextField;
	private JTextField houseOwnerIPTextField;
	private JTextField OppIPTextField;
	private JTextField houseOwnerPortTextField;
	private JTextField OppPortTextField;
	private JTextArea sendText;
	private JTextArea receiveText;
	private JLabel lblIsBegin;
	private Stack<MessageLog> sendMessageStack=new Stack<MessageLog>();
	private Stack<MessageLog> receiveMessageStack=new Stack<MessageLog>();
	
	static Vector<String> clientsMessage = new Vector<String>(20);
	 JList clientsList= new JList(clientsMessage);
	 private JTextField threadTextField;
	 private JTextField isRunTextField;

	/**
	 * Create the frame.
	 */
	public ServerJFrame(ServerOfLAN s) {
		server = s;
		setTitle("五子棋服务器");
		setBounds(100, 100, 894, 671);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel ServerMessagePanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) ServerMessagePanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(ServerMessagePanel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("IP：");
		lblNewLabel.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(lblNewLabel);
		
		serverIPTextField = new JTextField();
		serverIPTextField.setFont(new Font("\u65B0\u5B8B\u4F53", serverIPTextField.getFont().getStyle(), 15));
		ServerMessagePanel.add(serverIPTextField);
		serverIPTextField.setColumns(20);
		
		Component horizontalStrut = Box.createHorizontalStrut(40);
		ServerMessagePanel.add(horizontalStrut);
		
		JLabel lblNewLabel_1 = new JLabel("端口：");
		lblNewLabel_1.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(lblNewLabel_1);
		
		serverPortTextField = new JTextField();
		serverPortTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(serverPortTextField);
		serverPortTextField.setColumns(10);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(40);
		ServerMessagePanel.add(horizontalStrut_1);
		
		JLabel lblNewLabel_2 = new JLabel("状态：");
		lblNewLabel_2.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(lblNewLabel_2);
		
		isWorkTextField = new JTextField();
		isWorkTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(isWorkTextField);
		isWorkTextField.setColumns(10);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(40);
		ServerMessagePanel.add(horizontalStrut_2);
		
		JLabel lblNewLabel_3 = new JLabel("人数：");
		lblNewLabel_3.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(lblNewLabel_3);
		
		clientNumTextField = new JTextField();
		clientNumTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		ServerMessagePanel.add(clientNumTextField);
		clientNumTextField.setColumns(8);
		
		JPanel MainPanel = new JPanel();
		contentPane.add(MainPanel, BorderLayout.CENTER);
		MainPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel RoomMessagePanel = new JPanel();
		MainPanel.add(RoomMessagePanel, BorderLayout.NORTH);
		RoomMessagePanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel HousOwnerPanel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) HousOwnerPanel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		RoomMessagePanel.add(HousOwnerPanel);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(40);
		HousOwnerPanel.add(horizontalStrut_3);
		
		JLabel lblNewLabel_4 = new JLabel("房主：");
		lblNewLabel_4.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(lblNewLabel_4);
		
		houseOwnerTextField = new JTextField();
		houseOwnerTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(houseOwnerTextField);
		houseOwnerTextField.setColumns(10);
		
		JLabel lblIp = new JLabel("IP：");
		lblIp.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(lblIp);
		
		houseOwnerIPTextField = new JTextField();
		houseOwnerIPTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(houseOwnerIPTextField);
		houseOwnerIPTextField.setColumns(20);
		
		JLabel lblNewLabel_6 = new JLabel("端口：");
		lblNewLabel_6.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(lblNewLabel_6);
		
		houseOwnerPortTextField = new JTextField();
		houseOwnerPortTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(houseOwnerPortTextField);
		houseOwnerPortTextField.setColumns(10);
		
		lblIsBegin = new JLabel(" ");
		lblIsBegin.setFont(new Font("新宋体", Font.PLAIN, 15));
		HousOwnerPanel.add(lblIsBegin);
		
		JPanel OppPanel = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) OppPanel.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		RoomMessagePanel.add(OppPanel);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(40);
		OppPanel.add(horizontalStrut_4);
		
		JLabel lblNewLabel_5 = new JLabel("对手：");
		lblNewLabel_5.setFont(new Font("新宋体", Font.PLAIN, 15));
		OppPanel.add(lblNewLabel_5);
		
		OppTextField = new JTextField();
		OppTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		OppPanel.add(OppTextField);
		OppTextField.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("IP：");
		lblNewLabel_7.setFont(new Font("新宋体", Font.PLAIN, 15));
		OppPanel.add(lblNewLabel_7);
		
		OppIPTextField = new JTextField();
		OppIPTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		OppPanel.add(OppIPTextField);
		OppIPTextField.setColumns(20);
		
		JLabel lblNewLabel_8 = new JLabel("端口：");
		lblNewLabel_8.setFont(new Font("新宋体", Font.PLAIN, 15));
		OppPanel.add(lblNewLabel_8);
		
		OppPortTextField = new JTextField();
		OppPortTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		OppPanel.add(OppPortTextField);
		OppPortTextField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		MainPanel.add(panel_1, BorderLayout.EAST);
		
		JPanel panel = new JPanel();
		MainPanel.add(panel, BorderLayout.WEST);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.NORTH);
		
		Component horizontalStrut_10 = Box.createHorizontalStrut(10);
		panel_2.add(horizontalStrut_10);
		
		JLabel lblNewLabel_10 = new JLabel("昵称");
		lblNewLabel_10.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_2.add(lblNewLabel_10);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(50);
		panel_2.add(horizontalStrut_5);
		
		JLabel lblNewLabel_11 = new JLabel("IP");
		lblNewLabel_11.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_2.add(lblNewLabel_11);
		
		Component horizontalStrut_6 = Box.createHorizontalStrut(50);
		panel_2.add(horizontalStrut_6);
		
		JLabel lblNewLabel_12 = new JLabel("端口");
		lblNewLabel_12.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_2.add(lblNewLabel_12);
		
		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		panel_2.add(horizontalStrut_7);
		
		JLabel lblNewLabel_13 = new JLabel("属性");
		lblNewLabel_13.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_2.add(lblNewLabel_13);
		
		Component horizontalStrut_9 = Box.createHorizontalStrut(15);
		panel_2.add(horizontalStrut_9);
		
		JPanel clientsListPanel = new JPanel();
		panel.add(clientsListPanel, BorderLayout.CENTER);
		clientsListPanel.setBorder(new LineBorder(Color.BLACK));
		clientsListPanel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane clientScrollPane = new JScrollPane();
		clientsListPanel.add(clientScrollPane, BorderLayout.CENTER);
		
		clientsList.setFont(new Font("新宋体", Font.PLAIN, 15));
		clientsList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				sendTextUpData();
				receiveTextUpData();
			}
		});
		clientScrollPane.setViewportView(clientsList);
		
		JPanel panel_6 = new JPanel();
		clientsListPanel.add(panel_6, BorderLayout.SOUTH);
		panel_6.setLayout(new GridLayout(0, 1, 2, 0));
		
		JPanel panel_7 = new JPanel();
		panel_6.add(panel_7);
		
		JLabel lblNewLabel_9 = new JLabel("线程：");
		panel_7.add(lblNewLabel_9);
		lblNewLabel_9.setFont(new Font("新宋体", Font.PLAIN, 15));
		
		threadTextField = new JTextField();
		panel_7.add(threadTextField);
		threadTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		threadTextField.setColumns(27);
		
		JPanel panel_8 = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) panel_8.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		panel_6.add(panel_8);
		
		Component horizontalStrut_11 = Box.createHorizontalStrut(1);
		panel_8.add(horizontalStrut_11);
		
		JLabel lblNewLabel_16 = new JLabel("状态：");
		lblNewLabel_16.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_8.add(lblNewLabel_16);
		
		isRunTextField = new JTextField();
		isRunTextField.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_8.add(isRunTextField);
		isRunTextField.setColumns(10);
		
		JPanel panel_5 = new JPanel();
		panel.add(panel_5, BorderLayout.EAST);
		
		Component horizontalStrut_8 = Box.createHorizontalStrut(10);
		panel_5.add(horizontalStrut_8);
		
		JPanel messagePanel = new JPanel();
		MainPanel.add(messagePanel, BorderLayout.CENTER);
		messagePanel.setLayout(new GridLayout(1, 0, 20, 0));
		
		JPanel sendPanel = new JPanel();
		messagePanel.add(sendPanel);
		sendPanel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane sendScrollPane = new JScrollPane();
		sendPanel.add(sendScrollPane);
		
		sendText = new JTextArea();
		sendText.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		sendScrollPane.setViewportView(sendText);
	    sendText.setLineWrap(true);// 激活自动换行功能
		 sendText.setWrapStyleWord(true);// 激活断行不断字功能
		 sendText.setTabSize(4);
		
		JPanel panel_3 = new JPanel();
		sendPanel.add(panel_3, BorderLayout.NORTH);
		
		JLabel lblNewLabel_14 = new JLabel("客户给服务器发送的信息");
		lblNewLabel_14.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_3.add(lblNewLabel_14);
		
		JPanel receivePanel = new JPanel();
		messagePanel.add(receivePanel);
		receivePanel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane receiveScrollPane = new JScrollPane();
		receivePanel.add(receiveScrollPane);
		
		receiveText = new JTextArea();
		receiveText.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		receiveScrollPane.setViewportView(receiveText);
	    receiveText.setLineWrap(true);// 激活自动换行功能
	    receiveText.setWrapStyleWord(true);// 激活断行不断字功能
	    receiveText.setTabSize(4);
		
		JPanel panel_4 = new JPanel();
		receivePanel.add(panel_4, BorderLayout.NORTH);
		
		JLabel lblNewLabel_15 = new JLabel("客户从服务器接收到的信息");
		lblNewLabel_15.setFont(new Font("新宋体", Font.PLAIN, 15));
		panel_4.add(lblNewLabel_15);
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (server.getIsWork()) {
					upData();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		}.start();
	}
	void upData(){
		try{
		 serverIPTextField.setText(server.getClientdataVec().get(0).getClientAddress().getHostAddress());
		 isWorkTextField.setText((server.getIsWork()? "正常工作":"停止工作"));
		 clientNumTextField.setText(server.getClientdataVec().size()+"");
		}catch(Exception e){
			e.printStackTrace();
		}
		 try {
			 houseOwnerTextField.setText(server.getClientdataVec().get(0).getClientName());
			 houseOwnerIPTextField.setText(server.getClientdataVec().get(0).getClientAddress().getHostAddress());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		 
		 try {
			 OppTextField.setText(server.getClientdataVec().get(1).getClientName());
			 OppIPTextField.setText(server.getClientdataVec().get(1).getClientAddress().getHostAddress());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		 try {
			 lblIsBegin.setText(server.isBegin() ? "比赛进行中……":"等待中……");
			 clientsMessage.clear();
			 clientsListUpData();
			 sendTextUpData();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		 try {
			 receiveTextUpData();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	void  clientsListUpData(){
		 String clientMessageString;
		 for(ClientDataClass clientDataClass: server.getClientdataVec()){
			 clientMessageString= returnName(clientDataClass.getClientName())+clientDataClass.getClientAddress().getHostAddress()+"     "
					+ clientDataClass.getAttribute()+"   " +clientDataClass.getID();
			 clientsMessage.add(clientMessageString);
		 }
		 clientsList.updateUI();
	 }
	String returnSpace(int n){
		String space= new String(" ");
		for(int i=0; i< n-1; i++){
			space +=" ";
		}
		return space;
	}
	
   String returnName(String name){
		int len=name.length();
		if(len <= 4){
			name= (returnSpace(5-len)+name+returnSpace(5-len));
		}
		else {
			name =" "+name+" ";
		}
		return name;
	}
   void sendTextUpData(){
	   sendText.setText("");
	   int index =clientsList.getSelectedIndex();
	   if(index >= 0){
		   ClientDataClass clientDataClass = server.getClientdataVec().get(index);
		   for(ServerForRoomClientOfLAN s : server.getServerThread()){//找到对应的线程
			   if(clientDataClass.equals(s.client)){
				   threadTextField.setText(s.toString());
				   isRunTextField.setText(s.isRun ? "正在运行中":"停止运行");
				   receiveMessageStack=s.receiveMessageStack;
				   if(receiveMessageStack !=null && !receiveMessageStack.isEmpty()){
					   for(MessageLog m : receiveMessageStack){
						   sendText.append(m.toString());
					   }
				   } 
			   }
		   }//找到对应的线程end
	   }
   }//sendTextUpData() end 
   
  void receiveTextUpData(){
	  receiveText.setText("");
	  int index =clientsList.getSelectedIndex();
	   if(index >= 0){
		   ClientDataClass clientDataClass = server.getClientdataVec().get(index);
		   for(ServerForRoomClientOfLAN s : server.getServerThread()){//找到对应的线程
			   if(clientDataClass.equals(s.client)){
				   threadTextField.setText(s.toString());
				   isRunTextField.setText(s.isRun ? "正在运行中":"停止运行");
				   sendMessageStack=s.sendMessageStack;
				   if(sendMessageStack !=null && !sendMessageStack.isEmpty()){
					   for(MessageLog m : sendMessageStack){
						   receiveText.append(m.toString());
					   }
				   } 
			   }
		   }//找到对应的线程end
	   }
  }//receiveTextUpData() end 
}


