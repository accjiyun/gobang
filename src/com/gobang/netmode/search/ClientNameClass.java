﻿package com.gobang.netmode.search;

public class ClientNameClass {
	static String[] clientNamedefine={  "司马" ,"上官", "欧阳", "夏侯" ,"诸葛", "闻人",
		"东方" ,"尉迟", "公羊", "宗政", "淳于", "单于", "太叔", "申屠", "公孙", "仲孙", "令狐",
		 "钟离", "宇文" ,"长孙" ,"鲜于" , "司徒" ,"司空", "亓官", "司寇" ,"子车","端木",  "公西",
		 "乐正",  "公良",  "夹谷", "宰父", "谷段干", "百里" ,"东郭" ,"南门", "呼延" , "微生" , "梁丘",
		 "左丘", "东门", "西门" ,"南宫", "关羽" ,"金轩", "金元" ,"金宇", "金允",  "金凌", "金承安",
		 "金承泽", "金承恩", "金承昊","向天颖", "苏苒苒", "莫萘",  "金泽城" ,"金泽宇", "金泽风" ,"金泽耀" ,
		 "金泽佑" ,"周显荣" ,"王敏芝", "周惠然", "周惠仙" ,"周芷慕","周芷堇" ,"周芷菁", "周芷莘", "沈从文", 
		 "沈从武", "沈代云" , "沈寄", "沈艺" ,"沈菡" ,"沈若棠", "沈青山", "沈未凝" , "欧阳修" ,"沈雁山" ,
		 "沈清和", "沈未央" ,"张子瑜", "张若曦", "张芸曦", "张正熙" , "张在熙", "张元熙" ,"萧沛儿", "萧宣", 
		 "萧慕青" ,"萧翊萱", "萧笙" ,"萧和",  "萧静萱", "萧芸萱" ,"夏安歌" ,"夏安然", "夏寄惜",
		  "夏颜惜", "刘书南", "刘安凝", "刘景儿", "刘易南", "刘易青", "刘毓真", "刘青云", "刘景云" ,"刘瑾璇", 
		  "筝江想容" ,"江云茉", "江梦瑶", "韩光熙", "黎欣", "南笙" ,"南萧" , "安阳", "安宜" ,"安盈" ,"安阳" , 
		  "安则妍" ,"苏拂" ,"苏笺云", "苏研月", "苏子陌", "金芙" , "崔影月" ,"花忆雨" ,"浅影梦清" ,"顾扶苏", 
		  "夏嫣然", "云伊裳" ,"莫离歌", "莫凉颜" ,"景瑟", "青璃", "楚越",  "倾城", "君青笑", "君莙" ,
		 "君青离", "之曼", "之桃" ,"之瑶", "忆柳" ,"忆香", "忆寒" ,"元芳" ,"惜梦" ,"惜柔" ,"惜文", "碧文",
		 "碧云", "香芹" ,"香寒", "香茹" ,"盼儿","春儿", "怜儿" ,"莲儿" ,"冬儿", "冬菱", "语蝶", "语琴", "语兰", 
		 "向彤", "碧彤", "妙彤", "冰彤", "以彤", "怜云" ,"诗云","以欣", "春竹", "春雁", "如风",
		  "如霜" ,"岚霜" ,"晓霜" , "元霜", "梦菡" ,"孙悟空" ,"梦蓉", "席烟" ,"若烟" ,"秋烟" ,"秋荷", "书巧" ,"书瑶",
		  "乐瑶", "新瑶", "夏瑶" ,"夏香" ,"夏彤" ,"凌瑶" ,"凌寒" ,"墨香", "墨兰", "琴雅", "琴兰", 
		 "书兰" ,"问兰", "尔兰", "访兰", "访琴", "碧菡", "碧云", "碧玉", "春文", "萧文", "絮柳" ,"絮锦",
		  "映容", "映文" ,"刘备" ,"慕岚" ,"香菱", "念春", "梦夏" ,"初香", "问梅", "采梅", "向雪", "听白",
		  "念瑶", "疏影", "怀蕊", "飘絮", "含烟", "琴和"};
	public static String getName(){
//		Random random = new Random();
//		int n=random.nextInt(clientNamedefine.length-1);
//		String nameString =clientNamedefine[n];
		int randomNumber = Math.abs((int)(Math.random()*clientNamedefine.length));
		String nameString =clientNamedefine[randomNumber];
//		System.err.println(randomNumber + nameString+ "******************************************");
		return nameString;
	}
}
