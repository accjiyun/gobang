﻿package com.gobang.netmode.sendandreceive;

public final class MessageMSG {
	public static final int CLIENT_DATA_CLASS = 1;
	public static final int OPP_CLIENT_DATA_CLASS = 2;
	public static final int CHAT_MSG = 4;
	public static final int IS_BEGIN = 8;
	public static final int CHESS_NUM = 16;
	public static final int CHESS_POINT = 32;
	public static final int CHESS_ALL = 64;
	public static final int ORDER = 128;
	public static final int CLIENTD_DATAS = 256;
	public static final int SERVER_MESSAGE_VECTOR = 512;
	public static final int ID_OF_CHESS = 1024;
	public static final int SERVER_MESSAGE = 2048;
	public static final int FILE = 4096;
}
