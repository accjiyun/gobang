﻿package com.gobang.netmode.sendandreceive;


public enum Order {
	SEARCH, 
	JOIN, 
	JOIN_FAIL,//加入房间失败
	EXIT,
	UPDATA,//刷新
	RECONECT,//重新连接
	APPLY_FIGHT,//申请备战
	CREATE_ROOM,//创建房间
	CREATE_ROOM_SUCCESS,//创建房间成功
	CREATE_ROOM_FAIL,//创建房间失败

	ASK_PEACE, // 求和
	BACK, // 悔棋
	GIVEUP, // 投降
	LAST_CHESS,//最好一颗棋子信息
	
	AGREE_ASKPEACE, // 同意
	DISAGREE_ASKPEACE, // 不同意
	AGREE_BACK, // 同意
	DISAGREE_BACK, // 不同意
	AGREE_GIVEUP, // 同意
	DISAGREE_GIVEUP, // 不同意
	SECRET_CHAT,//私聊
	CHAT,//群聊

	KICK, // 踢人
	KICKED,//被踢的人
	KICK_FAIL,//踢人失败
	BEGIN, // 开始游戏,只能由房主发出
	FILE,//文件

	AGREE_BEGIN, // 同意开始，只能由对手发出
	DISAGREE_BEGIN,
	WIN,//赢棋
	AGREE_REPLAY,
	DISAGREE_REPLAY,

	SET_BEGIN, // 设置对局双方
	SET_SERVER, // 设为服务器
	SETOPP,//设为对手
	ASK_ALL_CHESS, // 获取所有棋子请求
	ASK_ROOMMSG,//请求获得房间的消息
	ROOMMSG,//一个房间的信息
	ALL_ROOMMSG,//所有房间的信息
	ASK_ALL_CLIENTS,//请求获得所有客户信息
	UPDATA_YOUSELF,//更新自己的clientDataClass
	NULL
}