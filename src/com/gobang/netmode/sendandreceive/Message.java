﻿package com.gobang.netmode.sendandreceive;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Stack;
import java.util.Vector;

import javax.swing.text.StyledDocument;

import com.gobang.netmode.util.ClientDataClass;
import com.gobang.netmode.util.ServerMessage;
import com.gobang.util.chessdata.ChessPoint;

public class Message implements Serializable {

	private static final long serialVersionUID = 1L;
	public int msg; // 用于翻译输入输出流对象
	public ClientDataClass clientDataClass;// 1
	public ClientDataClass oppClientDataClass;// 用于私聊信息的发送 2
	public StyledDocument chatmsg;// 4
	public boolean isBegin;// 棋盘是否开始// 8
	public int chessNum; // 目前的棋子数 16
	public ChessPoint chesspoint;// 最后一颗棋子的坐标 32
	public Stack<ChessPoint> chessall; // 棋子的堆栈 64
	/* 下面的信息有些奇特，要根据命令来提前信息 */
	public Order order; // 命令 128
	public Vector<ClientDataClass> clientDatas; // 所有客户的信息 256
	public Vector<ServerMessage> serverMessageVector; // 所有房间的信息 512
	public String IDOfChess;// 每一盘棋的ID， 由时间构成 1024
	public ServerMessage serverMessage;
	public MyFile file;// 传输的文件，可以是图片
	public long time;

	public Message() {
		this.msg = 0; // 用于翻译输入输出流对象
		// 解析出来的信息
		clientDataClass = null;
		oppClientDataClass = null;
		this.chatmsg = null;
		this.isBegin = false;
		this.chessNum = 0; // 目前的棋子数
		this.chesspoint = null;// 最后一颗棋子的坐标
		this.chessall = null; // 棋子的堆栈
		/* 下面的信息有些奇特，要根据命令来提前信息 */
		this.order = Order.NULL; // 命令
		clientDatas = null;
		serverMessageVector = null;
		IDOfChess = null;
		serverMessage = null;
		file = null;
	}
	

	public long getTime() {
		return time;
	}


	public void setTime(long time) {
		this.time = time;
	}
	public void setTime() {
		this.time = System.currentTimeMillis();
	}
	public ClientDataClass getClientDataClass() {
		return clientDataClass;
	}

	public void setClientDataClass(ClientDataClass clientDataClass) {
		this.clientDataClass = clientDataClass;
	}

	public ClientDataClass getOppClientDataClass() {
		return oppClientDataClass;
	}

	public void setOppClientDataClass(ClientDataClass oppClientDataClass) {
		this.oppClientDataClass = oppClientDataClass;
	}

	public ChessPoint getChesspoint() {
		return chesspoint;
	}

	public void setChesspoint(ChessPoint chesspoint) {
		this.chesspoint = chesspoint;
	}

	public Stack<ChessPoint> getChessall() {
		return chessall;
	}

	public void setChessall(Stack<ChessPoint> chessall) {
		this.chessall = chessall;
	}

	public Vector<ClientDataClass> getClientDatas() {
		return clientDatas;
	}

	public void setClientDatas(Vector<ClientDataClass> clientDatas) {
		this.clientDatas = clientDatas;
	}

	public Message(int msg) {// 构造方法
		this.msg = msg;
		this.chatmsg = null;
		this.isBegin = false;
		this.chessNum = 0; // 目前的棋子数
		this.chesspoint = null;// 最后一颗棋子的坐标
		this.chessall = null; // 棋子的堆栈
		/* 下面的信息有些奇特，要根据命令来提前信息 */
		this.order = Order.NULL; // 命令

	}

	public Message(int msg, Vector<ClientDataClass> clientDatas) {// 构造方法
		this.msg = msg;
		this.chatmsg = null;
		this.clientDatas = clientDatas;
	}

	public boolean setFile(String fileAbsolutePath) {
		if (file != null) // 已经新建了
		{
			return file.setFileBuf(fileAbsolutePath);
		} else {// 重新新建
			file = new MyFile();
			return file.setFileBuf(fileAbsolutePath);
		}
	}
	public boolean saveFile() {
		if (file != null && file.length > 0) {
			String filePath = System.getProperty("java.io.tmpdir") +"gobangtemp";
			File f = new File(filePath);
			if(!f.exists()  && !f .isDirectory()){     
				f.mkdir();  
			}
			String fileAbsolutePath = filePath + "\\"+file.getFileName();
			System.err.println(fileAbsolutePath);
			f = new File(fileAbsolutePath);
			file.setFileName(f.getName());
			try {
				FileOutputStream	fos = new FileOutputStream(f);
				BufferedOutputStream	bos = new BufferedOutputStream(fos);
				bos.write(file.filebuf);
				bos.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			return true;	
		}
		return false;
	}

	/*
	 *
	 * public static void showChatIntable(String Message, String PlayerName，
	 * URL[] imagesPath, int[] offsetImages)
	 * 
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String MessageString = new String("");
		MessageString += ("msg: " + msg + "  时间 ： "+time+"\n");
		if (clientDataClass != null) {
			MessageString += ("clientDataClass: " + clientDataClass);
		}
		if (oppClientDataClass != null) {
			MessageString += ("oppClientDataClass: " + oppClientDataClass);
		}
		MessageString += ("聊天记录：" + chatmsg + "\n");
		MessageString += ("比赛是否开始：" + isBegin + "\n");
		MessageString += ("棋盘的ID: " + IDOfChess + "\n");
		if (chesspoint != null) {
			MessageString += ("第几颗棋子: " + chessNum + "  chesspoint: " + chesspoint);
		}
		MessageString += ("棋子的堆栈: ");
		if (chessall != null && !chessall.empty()) {
			MessageString += "\n";
			for (ChessPoint chessPoint : chessall) {
				MessageString += chessPoint;
			}
		} else {
			MessageString += "null\n";
		}
		MessageString += ("命令：" + order + "\n");
		MessageString += ("客户堆栈:");
		if (clientDatas != null) {
			MessageString += ("\n");
			for (ClientDataClass clientDataClass : clientDatas) {
				MessageString += clientDataClass;
			}
			MessageString += ("\n");
		} else {
			MessageString += ("null");
		}
		MessageString += ("房间信息列表：[");
		if (serverMessageVector != null) {
			MessageString += ("\n");
			for (ServerMessage s : serverMessageVector) {
				MessageString += s;
			}
			MessageString += ("\t]\n");
		} else {
			MessageString += ("null");
		}
		MessageString += ("房间信息：");
		if (serverMessage != null) {
			MessageString += serverMessage;
		} else {
			MessageString += ("null\n");
		}
		MessageString += ("\n");
		return MessageString;
	}

	public StyledDocument getChatmsg() {
		return chatmsg;
	}

	public void setChatmsg(StyledDocument chatmsg) {
		this.chatmsg = chatmsg;
	}

	public class MyFile implements Serializable {
		String fileName;
		int length;
		byte[] filebuf;

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public byte[] getFilebuf() {
			return filebuf;
		}

		public void setFilebuf(byte[] filebuf) {
			this.filebuf = filebuf;
		}

		public int getLength() {
			return length;
		}

		public boolean setFileBuf(String fileAbsolutePath) {
			File file = new File(fileAbsolutePath);
			length = (int) file.length();
			if (length > 10485760 || length <= 0) {// 小于10M
				length = 0;
				return false;
			}
			filebuf = new byte[length];
			try {
				FileInputStream fis = new FileInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(fis);
				bis.read(filebuf);
				bis.close();
				fileName = file.getName();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			return true;
		}

		public String toString() {
			// TODO Auto-generated method stub
			String string = new String("[");
			if (filebuf != null) {
				string += ("文件名: " + fileName);
				string += ("文件长度: " + length + "\t");
			}
			string += "]";
			return string;
		}
	}

}
