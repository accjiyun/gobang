﻿package com.gobang.netmode.util;

import java.io.Serializable;
import java.net.InetAddress;

public class ServerMessage implements Serializable{
	private static final long serialVersionUID = 1L;
	String ID;//保存的是服务器的ID,便于区别其他的服务器，并更改里面的的数值，创建之后无法更改；
	InetAddress serverAddress;
	String houseOwnerNameString;
	InetAddress oppAddress;
	String oppNameString;
	int roomNum;
	boolean isBegin;
	int port;//服务器的端口
	public ServerMessage () {
		// TODO Auto-generated constructor stub
		this.ID = randomID();
		this.serverAddress = null;
		this.houseOwnerNameString= null;
		this.oppAddress = null;
		this.oppNameString= null;
		this.roomNum= 0;
		this.isBegin= false;
		this.port = 26457;
	}
	public ServerMessage (ClientDataClass houseOwnClient) {
		// TODO Auto-generated constructor stub
		this.ID = randomID();
		this.serverAddress = houseOwnClient.clientAddress;
		this.houseOwnerNameString= houseOwnClient.clientName;
		this.oppAddress = null;
		this.oppNameString= null;
		this.roomNum= 0;
		this.isBegin= false;
		this.port = 0;
		
	}
	public ServerMessage(String ID, InetAddress serverAddress, String houseOwnerNameString, InetAddress oppAddress, String oppNameString, int roomNum, boolean isBegin, int port) {
		this.ID = ID;
		this.serverAddress = serverAddress;
		this.houseOwnerNameString= houseOwnerNameString;
		this.oppAddress = oppAddress;
		this.oppNameString= oppNameString;
		this.roomNum= roomNum;
		this.isBegin= isBegin;
		this.port = port;
	}
	public ServerMessage(InetAddress serverAddress, String houseOwnerNameString, int port ){
		this.ID = randomID();
		this.serverAddress = serverAddress;
		this.houseOwnerNameString= houseOwnerNameString;
		this.port = port;
	}
	public InetAddress getServerAddress(){
		return serverAddress;
	}
	public String getHouseOwnerName(){
		return houseOwnerNameString;
	}
	public InetAddress getOppAddress() {
		return oppAddress;
	}
	public String getOppNameString() {
		return oppNameString;
	}
	public int  getroomNum() {
		return roomNum;
	}
	public int getPort() {
		return port;
	}
	public String getID() {
		return ID;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public boolean equals(String ID) {
		// TODO Auto-generated method stub
		return this.ID.equals(ID);
	}
	 public void setRoomNum(int roomNum) {
		this.roomNum = roomNum;
	}
	 
	public boolean isBegin() {
		return isBegin;
	}
	public void setBegin(boolean isBegin) {
		this.isBegin = isBegin;
	}
	
	public String getHouseOwnerNameString() {
		return houseOwnerNameString;
	}
	public void setHouseOwnerNameString(String houseOwnerNameString) {
		this.houseOwnerNameString = houseOwnerNameString;
	}
	public int getRoomNum() {
		return roomNum;
	}
	public void setServerAddress(InetAddress serverAddress) {
		this.serverAddress = serverAddress;
	}
	public void setOppAddress(InetAddress oppAddress) {
		this.oppAddress = oppAddress;
	}
	public void setOppNameString(String oppNameString) {
		this.oppNameString = oppNameString;
	}
	@Override
	public String toString() {
			String string ="[";
			if(ID != null){
				string +=("房间ID: "+ID+"\n");
			}
			if(houseOwnerNameString != null){
				string +=("房主: "+houseOwnerNameString+"\n");
			}
			if(serverAddress != null){
				string +=("房主地址: "+serverAddress+"\n");
			}
			if(oppNameString != null){
				string +=("对手: "+oppNameString+"\n");
			}
			if(oppAddress != null){
				string +=("对手地址: "+oppAddress+"\n");
			}
			string +=("人数: "+roomNum +",比赛是否开始: "+isBegin+",服务器端口: "+port+"]\n");
			return string;
	}
	public String randomID() {
		String ID = "";
		for (int i = 0; i < 16; i++) {
			ID += (char) (Math.abs((int) (Math.random() * 26)) + 65);
		}
		ID += System.currentTimeMillis();
		return ID;
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return this.ID.equals( ((ServerMessage )obj).getID());
	}
	
}
