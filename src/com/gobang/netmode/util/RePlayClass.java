﻿package com.gobang.netmode.util;

public class RePlayClass {
	//数值为2表示没有回应，0表示不同意，1表示同意
	int houseOwner;
	int opponent;
	public RePlayClass() {
		// TODO Auto-generated constructor stub
		this.houseOwner=2;
		this.opponent=2;
	}
	public int getHouseOwner() {
		return houseOwner;
	}
	public void setHouseOwner(int houseOwner) {
		this.houseOwner = houseOwner;
	}
	public int getOpponent() {
		return opponent;
	}
	public void setOpponent(int opponent) {
		this.opponent = opponent;
		
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String string = new String("");
		string += ("[HouseOwner: "+houseOwner);
		string += (",Opponent"+opponent+"]\n");
		return string;
	}
	public void init() {
		this.houseOwner=2;
		this.opponent=2;
	}
}
