﻿package com.gobang.netmode.util;

import java.io.Serializable;
import java.net.InetAddress;

public class ClientDataClass implements Serializable,
		Comparable<ClientDataClass> {
	/**
	 * 为了适应不同的网络环境，用于连接的套接字可能随时被更换，所以识别客户身份将用一串字符来代替
	 */
	private static final long serialVersionUID = 1L;
	String clientName;
	InetAddress clientAddress;
	int attribute;
//	int localPort;
	String ID;//IP+时间+随机数
	public ClientDataClass() {
		// TODO Auto-generated constructor stub
		this.clientName = "张飞";
		this.clientAddress = null;
		this.attribute = 0;
		this.ID = null;
	}

	public ClientDataClass(String clientName, InetAddress clientAddress,
			int attribute) {
		// TODO Auto-generated constructor stub
		this.clientName = clientName;
		this.clientAddress = clientAddress;
		this.attribute = attribute;
		this.ID = null;
	}

	public ClientDataClass(String clientName, InetAddress clientAddress,
			int attribute, String  ID) {
		// TODO Auto-generated constructor stub
		this.clientName = clientName;
		this.clientAddress = clientAddress;
		this.attribute = attribute;
		this.ID = ID;
	}

	public void setClientName(String Name) {
		clientName = new String(Name);
	}

	public void setClientAddress(InetAddress address) {
		clientAddress = address;
	}

	public void setAttribute(int a) {
		attribute = a;
	}

	public String getClientName() {
		return clientName;
	}

	public InetAddress getClientAddress() {
		return clientAddress;
	}

	public int getAttribute() {
		return attribute;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String string = new String("["+"客户姓名: " + getClientName() + "  客户IP: "
				+ getClientAddress() + "  客户属性: " + getAttribute()
				+ "  ID号: " +  getID()+"]\n");
		return string;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || this == null || this.ID == null ||((ClientDataClass)obj).getID() == null){
			System.out.println("ClientDataClass.equals() null this:"+this +"   obj:"+obj);
			System.out.println("ClientDataClass.equals() null this.ID:"+this.ID +"   ((ClientDataClass)obj).getID():"+((ClientDataClass)obj).getID());
			return false;
		}
		// TODO Auto-generated method stub
		if(this.ID.equals( ((ClientDataClass)obj).getID()) ){
			return true;
		}else {
			return false;
		}
	}
	public int compareTo(ClientDataClass c) {
		// TODO Auto-generated method stub
		return this.attribute > c.attribute ? 1
				: (this.attribute == c.attribute ? 0 : -1);
	}
}